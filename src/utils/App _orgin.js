import { StatusBar } from "expo-status-bar";
import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  ActivityIndicator,
  Platform,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { AppBar } from "../components/AppBar";
import { Question } from "../features/question/Question";
import { Login } from "../features/Login/login";
import { SignUp } from "../features/Login/Signup";
import { Verify } from "../features/Login/verify";
import { MainNav } from "../features/maindasbboard/screen/mainnav";

export default function App() {
  global.apiUrl = "http://192.168.1.130:3000/";
  const [view, setView] = new useState();
  const [userId, setUserId] = useState();
  const [isLoading, setLoading] = useState(false);
  const [question, setQuestion] = useState([]);
  console.log("---------------Start Loading------------------------");
  console.log("----------------------------------------------------");
  console.log(" On Load View is ", view, " UserId", userId);

  useEffect(() => {
    //loadUserId();
    //loadView();
    //getClear();
    //logout();
    loadMvc();
  }, []);

  useEffect(() => {
    console.log("---------------View Effected------------------------");
    console.log("-------------------------------------------------------");
    if (view === "logout") {
      //console.log("The Value is -1 Lets Logout of the App");
      logout();
    } else {
      saveView(view);
    }
  }, [view]);

  useEffect(() => {
    console.log("---------------User Id Effected------------------------");
    console.log("-------------------------------------------------------");
    if (userId === "-1") {
      console.log("The Value is -1 Lets Logout of the App");
      logout();
    } else {
      console.log("The Value is", userId, " let Continue");
      saveUserId(userId);
    }
  }, [userId]);

  const loadUserId = async () => {
    let ui = await AsyncStorage.getItem("@youruserId");
    if (ui !== null) {
      setUserId(ui);
      //removeValue();
    } else {
      setUserId(null);
      //console.log("No val");
      //removeValue();
    }
  };
  const loadView = async () => {
    let viewVal = await AsyncStorage.getItem("@yourstatus");
    if (viewVal !== null) {
      setView(viewVal);
    } else {
      //setView("Login");
      //removeValue();
    }
  };

  const loadMvc = async () => {
    let values;
    try {
      values = await AsyncStorage.multiGet(["@youruserId", "@yourstatus"]);
      //console.log(values[0]);
      //console.log(values[0][1]);
      let user = values[0][1];
      let v = values[1][1];
      if (user === null) {
        setUserId("-1");
      }
      if (v === null) {
        setUserId("-1");
      }
      console.log("View", v, "User Value", user);
    } catch (e) {}
  };

  const removeValue = async () => {
    try {
      await AsyncStorage.multiRemove(["@youruserId", "@yourstatus"]);
      //await AsyncStorage.clear();
    } catch (e) {
      console.log("Error while removing Data", e);
      //remove error
    }

    //console.log("Done.");
  };

  const saveView = async (status) => {
    try {
      let user = await AsyncStorage.getItem("@yourstatus");
      if (user !== null) {
        await AsyncStorage.removeItem("@yourstatus");
      }
      if (status !== null && status !== undefined) {
        await AsyncStorage.setItem("@yourstatus", status);
      }
    } catch (e) {
      console.error(e);
    }
  };

  const saveUserId = async (id) => {
    try {
      let user = await AsyncStorage.getItem("@youruserId");
      if (user !== null) {
        await AsyncStorage.removeItem("@youruserId");
      }
      if (id !== null && id !== undefined) {
        await AsyncStorage.setItem("@youruserId", id);
      }
    } catch (e) {
      console.error(e);
    }
  };

  const logout = async () => {
    let user = await AsyncStorage.getItem("@youruserId");
    console.log("-----------User Id States------------");
    console.log(user);
    if (user === null) {
      await AsyncStorage.removeItem("@youruserId");
      setUserId(null);
    }
    let viewStatus = await AsyncStorage.getItem("@yourstatus");
    console.log("---------View State--------------");
    console.log(viewStatus);
    if (viewStatus === null) {
      await AsyncStorage.removeItem("@yourstatus");
      setView("Login");
    }
  };
  const getClear = async () => {
    let values;
    try {
      removeValue();
      values = await AsyncStorage.multiGet(["@youruserId", "@yourstatus"]);
      console.log(values);
    } catch (e) {}
  };

  const handleLogin = async (res) => {
    if (res.login) {
      setUserId(res.userId);
      //setUserIdVal(res.userId);
      let viewValue = res.status;
      setView(viewValue);
      //console.log("Value of ", viewValue);
      //console.log("Logn success ", res.status);
    } else {
      console.log("Login failed");
    }
  };

  console.log(" After Load View is ", view, " UserId", userId);
  console.log("---------------------The End------------------------------");

  /*const questionItem = {
    qnsNo: 1,
    qnsNoTotal: 10,
    qnsContent: "Testing the application View Screen",
    qnsAnswer: [
      { id: 1, answer: "Yes", nextQnsId: 2 },
      { id: 2, answer: "No", nextQnsId: 3 },
      { id: 3, answer: "I donot know", nextQnsId: 2 },
      { id: 4, answer: "Not the above", nextQnsId: 3 },
    ],
  };*/

  /*useEffect(() => {
    fetch("http://192.168.1.130:3000/questions/", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        qnsId: 1,
      }),
    })
      .then((response) => response.json())
      .then((json) => setQuestion(json.returnData))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);
*/
  /*
{userId ? (
        <Login />
      ) : (
        <>
          <Text>Hellow World</Text>
        </>
      )}
*/
  return (
    <SafeAreaView style={styles.fsContainer}>
      {userId !== null ? (
        view === "verify" ? (
          <Verify setview={setView} userId={userId} />
        ) : view === "verified" ? (
          <>
            <AppBar
              screenView="Justice Hive"
              setview={setView}
              setuserid={setUserId}
            />
            <MainNav userId={userId} setview={setView} />
          </>
        ) : view === "Login" ? (
          <></>
        ) : (
          <>
            <AppBar screenView="Page Not Found 404" />
            <MainNav />
          </>
        )
      ) : view === "Login" ? (
        <Login
          setview={setView}
          setuserId={setUserId}
          handleLogin={handleLogin}
        />
      ) : view === "SignUp" ? (
        <SignUp setview={setView} />
      ) : (
        <></>
      )}
    </SafeAreaView>
  );
}

/*
<Login />
isLoading? (
  <>
<AppBar screenView="Case Inquiry" />
<ActivityIndicator />
</>
) : (
<View style={styles.container}>
  <Question questionItem={question} />
  <StatusBar style="auto" />
</View>
)*/

const styles = StyleSheet.create({
  container: {
    flex: 0.5,
    backgroundColor: "#fff",
  },
  fsContainer: {
    flex: 1,
    marginTop: Platform.OS === "ios" ? 20 : 20,
  },
});
