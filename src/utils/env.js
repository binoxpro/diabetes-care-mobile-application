import { Platform } from "react-native";
const liveHost = "";
//const localhost = "http://192.168.1.130:3000/";
//const localhost = "http://192.168.100.4:3000/";
//const localhost = "https://dcaredemo.herokuapp.com/";
//const localhost = "http://192.168.1.130:3000/";
//const localhost = "http://192.168.1.130:3000/";
const localhost = "http://192.168.100.4:3000/";

export const isAndroid = Platform.OS === "android";
export const isDevelopment = (process.env.NODE_ENV = "development");
export const isMock = true;
export const host = localhost;
