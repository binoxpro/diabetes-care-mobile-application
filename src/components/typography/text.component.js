import styled from "styled-components/native";

const defaultTextStyles = (theme) => `
    font-family:${theme.fonts.body};
    font-weight:${theme.fontWeights.regular};
    color:${theme.colors.text.primary};
    flex-wrap:wrap;
    margin-top:0px;
    margin-bottom:0px;
`;

const body = (theme) => `
    font-size:${theme.fontSizes.body};
    
`;
const hint = (theme) => `
font-size:${theme.fontSizes.body};
`;
const error = (theme) => `
color:${theme.colors.text.error};
`;
const numlock = () => `
color: #e91e63;`;
const caption = (theme) => `
font-size:${theme.fontSizes.caption};
font-weight:${theme.fontWeights.bold};
`;
const title = (theme) => `
font-family:${theme.fonts.heading};
font-size:${theme.fontSizes.h4};
font-weight: ${theme.fontWeights.bold};
color:${theme.colors.text.primary}
`;
const titleDiet = (theme) => `
font-family:${theme.fonts.heading};
font-size:${theme.fontSizes.h5};
font-weight: ${theme.fontWeights.bold};
color:${theme.colors.text.primary}
`;
const label = (theme) => `
font-family:${theme.fonts.heading};
font-size:${theme.fontSizes.h3};
font-weight: ${theme.fontWeights.regular}
`;
const heading = (theme) => `
font-family:${theme.fonts.heading};
font-size:${theme.fontSizes.title};
font-weight: ${theme.fontWeights.medium};
`;
const lists = (theme) => `
font-family:${theme.fonts.heading};
font-size:${theme.fontSizes.body};
font-weight: ${theme.fontWeights.medium};
padding-left:5px;

`;
const listHeading = (theme) => `
justify-content:center;
`;

const variants = {
  body,
  label,
  caption,
  error,
  hint,
  title,
  heading,
  numlock,
  lists,
  listHeading,
  titleDiet,
};

export const Text = styled.Text`
  ${({ theme }) => defaultTextStyles(theme)}
  ${({ variant, theme }) => variants[variant](theme)}
`;

Text.defaultProps = {
  variant: "body",
};
