import { Card } from "react-native-paper";
import styled from "styled-components/native";

const defaultCardStyles = (theme) => `
margin: ${theme.sizes[0]};
`;

export const TestCard = styled(Card)`
  ${({ theme }) => defaultCardStyles(theme)}
`;


