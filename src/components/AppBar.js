import React from "react";
import { Appbar } from "react-native-paper";
import { StyleSheet, Text, SafeAreaView, Platform } from "react-native";

const MORE_ICON = Platform.OS === "ios" ? "dots-horizontal" : "dots-vertical";
export const AppBar = ({ screenView = "", setview = {}, setuserid = {} }) => {
  return (
    <Appbar.Header style={styles.bottom}>
      <Appbar.Content title={screenView} style={styles.title} />
      <Appbar.Action icon={MORE_ICON} />
      <Appbar.Action
        icon={"clipboard-arrow-right-outline"}
        onPress={() => {
          setview("Login");
          setuserid(-1);
        }}
      />
    </Appbar.Header>
  );
};
const styles = StyleSheet.create({
  bottom: {
    backgroundColor: "#8e44ad",
  },
  title: {
    alignItems: "center",
    justifyContent: "center",
    fontSize: 12,
    color: "blue",
  },
});
