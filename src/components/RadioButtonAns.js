import React from "react";
import { Text, View, StyleSheet } from "react-native";
import { RadioButton } from "react-native-paper";

export const RadioButtonAns = ({ itemName, onClick }) => {
  //console.log("Radio is " + itemName + ":");
  return (
    <View style={styles.container}>
      <RadioButton
        value={itemName}
        status="checked"
        onPress={() => {
          onClick;
        }}
      />
      <Text style={styles.btnText}>{itemName}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 0.2,
    flexDirection: "row",
    padding: 10,
  },
  btnText: {
    fontSize: 18,
    fontWeight: "bold",
  },
});
