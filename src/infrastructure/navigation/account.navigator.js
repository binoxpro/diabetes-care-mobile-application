import React, { useContext } from "react";

import { createStackNavigator } from "@react-navigation/stack";
import { LoginScreen } from "../../features/Login/login.screen";
import {SignUpScreen} from "../../features/Login/signup.screen";

const AccountStack = createStackNavigator();

export const AccountNavigator = () => {
  //console.log("Checking the Value of is verified", isVerified);
  return (
    // <AuthienticationContextProvider>
    <AccountStack.Navigator>
      <AccountStack.Screen
        name="login"
        component={LoginScreen}
        options={{ headerShown: false, headerTitle: "" }}
      />
      <AccountStack.Screen
        name="SignUp"
        component={SignUpScreen}
        options={{ headerShown: true, headerTitle: "Create Account" }}
      />
    </AccountStack.Navigator>
    // </AuthienticationContextProvider>
  );
};
