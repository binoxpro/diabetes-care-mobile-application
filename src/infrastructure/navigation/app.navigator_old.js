import React, { useState, useEffect, useContext } from "react";

import * as BackgroundFetch from "expo-background-fetch";
import * as TaskManager from "expo-task-manager";
import * as Location from "expo-location";
import { Text, View } from "react-native";

import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Ionicons } from "@expo/vector-icons";

import { AirwayBillContextProvider } from "../../services/airwaybill/airwaybill.context";
import { AiwayBillsNavigator } from "./airwaybills.navigator";
import { AirwayBillHistoryScreen } from "../../features/airwaybill/screens/airwaybillshistory.screen";
import { host } from "../../utils/env";
import { AuthenticationContext } from "../../services/authentication/authentication.context";
//import { CompleteAWContextProvider } from "../../services/airwaybill/completeaw.context";

const TAB_ICON = {
  Home: "home",
  Tests: "flask",
  Diet: "fast-food",
  Execrise:"bicycle",
  DConnect:"medical",
  Profile:"person",
  "Airway Bill": "ios-add-circle",

  History: "list",
  Incident: "information-circle",
};
const tabBarIcon =
  (iconName) =>
  ({ size, color }) =>
    <Ionicons name={iconName} size={size} color={color} />;
const screenOptions = ({ route }) => {
  const iconName = TAB_ICON[route.name];
  return {
    tabBarIcon: tabBarIcon(iconName),
    tabBarActiveTintColor: "tomato",
    tabBarInactiveTintColor: "gray",
  };
};

const BACKGROUND_FETCH_TASK = "background-fetch";

const AirwayBillsList = () => {
  return (
    // <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
    //   <Text>List of Allocated Shipments Jobs</Text>
    // </View>
    <AiwayBillsNavigator />
  );
};
const HistoryScreen = () => {
  return (
    // <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
    //   <Text>History Delivery Screen</Text>
    // </View>
    <AirwayBillHistoryScreen />
  );
};
const IncidentScreen = () => {
  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <Text>Incident Screen</Text>
    </View>
  );
};

const Tab = createBottomTabNavigator();
//const { onTracking } = useContext(AirwayBillContext);

export const AppNavigator = () => {
  const [location, setLocation] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);
  const { userId } = useContext(AuthenticationContext);

  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== "granted") {
        setErrorMsg("Permission to access location was denied");
        return;
      }

      let location = await Location.getCurrentPositionAsync({});
      setLocation(location);
    })();
    const interval = setInterval(() => {
      uploadDataAtInterval();
    }, 100000);
    return () => clearInterval(interval);
  }, []);

  const uploadDataAtInterval = async () => {
    //await getGPSPosition();
    let location = await Location.getCurrentPositionAsync({});
    setLocation(location);
    console.log(
      "upload using axios",
      location.coords.latitude,
      " ",
      location.coords.longitude
    );
    send(location.coords.latitude, location.coords.longitude, userId);
  };

  const send = async (lat, lng, userId) => {
    fetch(`${host}airWaybills/positionDriver/`, {
      method: "POST",
      headers: {
        Accept: "*/*",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        driverId: userId,
        lat: lat,
        lng: lng,
      }),
    }).then((response) => {
      return response.text();
    });
  };

  // useEffect(() => {
  //   (async () => {
  //     let { status } = await Location.requestForegroundPermissionsAsync();
  //     if (status !== "granted") {
  //       setErrorMsg("Permission to access location was denied");
  //       return;
  //     }

  //     let location = await Location.getCurrentPositionAsync({});
  //     setLocation(location);
  //   })();
  // }, []);

  // const backgroundLocationFetch = async () => {
  //   const { status } = await Location.requestPermissionsAsync();
  //   if (status === "granted") {
  //     console.log("cmon dance with me!");
  //     await Location.startLocationUpdatesAsync("FetchLocationInBackground", {
  //       accuracy: Location.Accuracy.Balanced,
  //       timeInterval: 10000,
  //       distanceInterval: 1,
  //       foregroundService: {
  //         notificationTitle: "Live Tracker",
  //         notificationBody: "Live Tracker is on.",
  //       },
  //     });
  //   }
  // };

  // let text = "Waiting..";
  // if (errorMsg) {
  //   text = errorMsg;
  // } else if (location) {
  //   text = JSON.stringify(location);
  // }
  // console.log(":>Location is", text);
  return (
    <AirwayBillContextProvider>
      <Tab.Navigator screenOptions={screenOptions}>
        <Tab.Screen name="Airway Bill" component={AirwayBillsList} />
        <Tab.Screen name="History" component={HistoryScreen} />
        <Tab.Screen name="Incident" component={IncidentScreen} />
      </Tab.Navigator>
    </AirwayBillContextProvider>
  );
};
