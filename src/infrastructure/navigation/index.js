import React, { useContext } from "react";

import { Text } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { AppNavigator } from "./app.navigator";
import { AccountNavigator } from "./account.navigator";
import { VerificationNavigator } from "./verfication.navigator";
import { AuthenticationContext } from "../../services/authentication/authentication.context";

export const Navigation = () => {
  const { auth } = useContext(AuthenticationContext);
  //console.log("@ the index nav file the value auth is ", auth);
  return (
    <NavigationContainer>
      {auth === "Login" ? (
        <AccountNavigator />
      ) : auth === "Success" ? (
        <AppNavigator />
      ) : (
        <Text>No a Thing</Text>
      )}
    </NavigationContainer>
  );
};
