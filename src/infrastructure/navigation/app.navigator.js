import React, { useState, useEffect, useContext } from "react";

import { Text, View } from "react-native";

import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Ionicons, Fontisto } from "@expo/vector-icons";

import { UserProfile } from "../../features/profile/screens/userProfile";
import { DtestScreen } from "../../features/dtest/screens/dtest.screen";
import { DTestNavigator } from "../../infrastructure/navigation/dtest.navigator";
import { ActiveDietScreen } from "../../features/diet/screen/activeDietScreen";
import { DietInfoCard } from "../../features/diet/components/diet-info-card.component";
import { SelectDietScreen } from "../../features/diet/screen/selectDiet.Screen";
import { ConnectWithDoctorScreen } from "../../features/dconnect/screen/ConectWithDoctor.Screen";
import { ConsultancyScreen } from "../../features/dconnect/screen/Consultancy.Screen";
import { StartSessionFrmScreen } from "../../features/dconnect/screen/StartSessionFrm.Screen";
import { StartSessionNavigator } from "../../features/dconnect/screen/startsession.navigator";

import { DietNavigator } from "../../features/diet/screen/diet.navigator"; //import { AirwayBillContextProvider } from "../../services/airwaybill/airwaybill.context";
//import { AiwayBillsNavigator } from "./airwaybills.navigator";
//import { AirwayBillHistoryScreen } from "../../features/airwaybill/screens/airwaybillshistory.screen";
//import { host } from "../../utils/env";
//import { AuthenticationContext } from "../../services/authentication/authentication.context";
import { ProfileContextProvider } from "../../services/profile/profile.context";
//import { CompleteAWContextProvider } from "../../services/airwaybill/completeaw.context";

const TAB_ICON = {
  Home: "home",
  Tests: "flask",
  Diet: "fast-food",
  Connect: "person-add",
  Consult: "add-circle-sharp",
  Profile: "person",
};
const tabBarIcon =
  (iconName) =>
  ({ size, color }) =>
    <Ionicons name={iconName} size={size} color={color} />;
const screenOptions = ({ route }) => {
  const iconName = TAB_ICON[route.name];
  return {
    tabBarIcon: tabBarIcon(iconName),
    tabBarActiveTintColor: "tomato",
    tabBarInactiveTintColor: "gray",
  };
};

const HomeScreen = () => {
  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <Text>Dashboard</Text>
    </View>
  );
};
const DoTestScreen = () => {
  return <DTestNavigator />;
};
const DietScreen = () => {
  return <DietNavigator />;
};

const ExecriseScreen = () => {
  return <ConnectWithScreen />;
};

const ProfileScreen = () => {
  return (
    // <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
    //   <Text>User Profile Screen</Text>
    // </View>
    <UserProfile />
  );
};

const DConnectScreen = () => {
  return <ConnectWithDoctorScreen />;
  //return <StartSessionFrmScreen />;
};

const ConsultScreen = () => {
  return <StartSessionNavigator />;
};

const Tab = createBottomTabNavigator();
//const { onTracking } = useContext(AirwayBillContext);

export const AppNavigator = () => {
  //const { userId } = useContext(AuthenticationContext);

  // useEffect(() => {
  //   (async () => {
  //     let { status } = await Location.requestForegroundPermissionsAsync();
  //     if (status !== "granted") {
  //       setErrorMsg("Permission to access location was denied");
  //       return;
  //     }

  //     let location = await Location.getCurrentPositionAsync({});
  //     setLocation(location);
  //   })();
  // }, []);

  // const backgroundLocationFetch = async () => {
  //   const { status } = await Location.requestPermissionsAsync();
  //   if (status === "granted") {
  //     console.log("cmon dance with me!");
  //     await Location.startLocationUpdatesAsync("FetchLocationInBackground", {
  //       accuracy: Location.Accuracy.Balanced,
  //       timeInterval: 10000,
  //       distanceInterval: 1,
  //       foregroundService: {
  //         notificationTitle: "Live Tracker",
  //         notificationBody: "Live Tracker is on.",
  //       },
  //     });
  //   }
  // };

  // let text = "Waiting..";
  // if (errorMsg) {
  //   text = errorMsg;
  // } else if (location) {
  //   text = JSON.stringify(location);
  // }
  // console.log(":>Location is", text);
  return (
    <ProfileContextProvider>
      <Tab.Navigator screenOptions={screenOptions}>
        <Tab.Screen name="Home" component={HomeScreen} />
        <Tab.Screen name="Connect" component={DConnectScreen} />
        <Tab.Screen name="Consult" component={ConsultScreen} />
        <Tab.Screen name="Tests" component={DoTestScreen} />
        <Tab.Screen name="Diet" component={DietScreen} />
        <Tab.Screen name="Profile" component={ProfileScreen} />
      </Tab.Navigator>
    </ProfileContextProvider>
  );
};
