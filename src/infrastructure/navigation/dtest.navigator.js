import React from "react";
import {
  createStackNavigator,
  TransitionPresets,
} from "@react-navigation/stack";
import { DtestScreen } from "../../features/dtest/screens/dtest.screen";
import { DtestCaptureDataScreen } from "../../features/dtest/screens/dtest-capturedata.screen";
import { DiabeticTestContextProvider } from "../../services/diabetictest/diabetictest.context";

const DtestStack = createStackNavigator();

export const DTestNavigator = () => {
  return (
    <DiabeticTestContextProvider>
      <DtestStack.Navigator headerShown="false">
        <DtestStack.Screen
          name="DiabeteTest"
          component={DtestScreen}
          options={{
            headerShown: false,
          }}
        />
        <DtestStack.Screen
          name="DiabeteTestCaptureData"
          component={DtestCaptureDataScreen}
          options={{
            headerShown: false,
          }}
        />
      </DtestStack.Navigator>
    </DiabeticTestContextProvider>
  );
};
