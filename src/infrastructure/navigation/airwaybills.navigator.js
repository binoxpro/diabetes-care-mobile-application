import React from "react";
import {
  createStackNavigator,
  TransitionPresets,
} from "@react-navigation/stack";
import { AirwayBillsScreen } from "../../features/airwaybill/screens/airwaybills.screen";
import { AirwayBillDetailScreen } from "../../features/airwaybill/screens/airwaybill-detail.screen";
import { AirwayBillSignturePadScreen } from "../../features/airwaybill/screens/airwaybill-signing.screen";
const AirwayBillStack = createStackNavigator();

export const AiwayBillsNavigator = () => {
  return (
    <AirwayBillStack.Navigator headerShown="false">
      <AirwayBillStack.Screen
        name="AirwayBills"
        component={AirwayBillsScreen}
        options={{
          headerShown: false,
        }}
      />
      <AirwayBillStack.Screen
        name="AirwayBillDetails"
        component={AirwayBillDetailScreen}
        options={{
          headerShown: false,
        }}
      />
      <AirwayBillStack.Screen
        name="AirwayBillSignaturePad"
        component={AirwayBillSignturePadScreen}
        options={{
          headerShown: false,
        }}
      />
    </AirwayBillStack.Navigator>
  );
};
