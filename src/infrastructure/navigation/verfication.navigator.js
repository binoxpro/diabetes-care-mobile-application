import React from "react";

import { createStackNavigator } from "@react-navigation/stack";
import { VerifyScreen } from "../../features/Login/verify.screen";
import { AuthienticationContextProvider } from "../../services/authentication/authentication.context";

const VerifictionStack = createStackNavigator();

export const VerificationNavigator = () => {
  //console.log("Checking the Value of is verified", isVerified);
  return (
    // <AuthienticationContextProvider>
    <VerifictionStack.Navigator>
      <VerifictionStack.Screen
        name="verify"
        component={VerifyScreen}
        options={{ headerTitle: "Authentication" }}
      />
    </VerifictionStack.Navigator>
    // </AuthienticationContextProvider>
  );
};
