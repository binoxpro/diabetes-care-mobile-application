import React from "react";
import { View, Text } from "react-native";
import {
  createStackNavigator,
  TransitionPresets,
} from "@react-navigation/stack";
import { Question } from "../../features/question/Question";
import { DemoCaseScreen } from "../../features/democase/screen/democase.screen";
const DemoCaseStack = createStackNavigator();

export const DemoCaseNavigator = () => {
  return (
    <DemoCaseStack.Navigator
      headerShown="false"
      screenOptions={{
        ...TransitionPresets.RevealFromBottomAndroid,
      }}
    >
      <DemoCaseStack.Screen
        name="Start"
        component={DemoCaseScreen}
        options={{
          headerShown: false,
        }}
      />
      <DemoCaseStack.Screen name="InquireDetail" component={Question} />
    </DemoCaseStack.Navigator>
  );
};
