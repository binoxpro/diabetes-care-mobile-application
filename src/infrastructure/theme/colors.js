export const colors = {
  brand: {
    primary: "#17a589",
    secondary: " #d4efdf",
    muted: " #e9f7ef",
  },
  ui: {
    primary: "#17a589",
    secondary: " #2e86c1 ",
    tertiary: "#F1F1F1",
    quaternary: "#FFFFFF",
    disabled: "#DEDEDE",
    error: "#D0421B",
    success: "#138000",
    pink: "#b71540",
  },
  bg: {
    primary: "#fdfefe",
    secondary: "#f8f9f9",
    tertiary:"#f2f4f4",
    disabled: "#1b2631",
    pink: "rgba(11, 83, 69, 0.1)",
    black: "#2C3E50",
  },
  text: {
    primary: " #0b5345",
    secondary: "#1b2631",
    tertiary:"#154360",
    disabled: "#1b2631",
    inverse: "#FFFFFF",
    error: "#D0421B",
    success: "#138000",
  },
};
