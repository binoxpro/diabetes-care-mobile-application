export const sizes = ["4px", "8px", "16px", "32px", "64px", "128px"];
export const buttonSizes = {
  lg: "80px",
  md: "60px",
  sm: "40px",
  lx: "20px",
  mx: "15px",
};
