import React, { createContext, useState } from "react";

export const DietContext = createContext();

export const DietContextProvider = ({ children }) => {
  const [refreshData, setRefreshData] = useState(false);
  const checkRefresh = () => {
    if (!refreshData) {
      setRefreshData(!refreshData);
    }
  };
  return (
    <DietContext.Provider value={(refreshData, checkRefresh)}>
      {children}
    </DietContext.Provider>
  );
};
