import { children } from "min-document";
import React, { useState, createContext, useEffect, useMemo } from "react";
import { questionRequest, questionTransform } from "./question.service";

export const QuestionContext = createContext();
export const QuestionContextProvider = ({ children }) => {
  const [questions, setQuestions] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  const retrieveQuestions = () => {
    setIsLoading(true);
    setTimeout(() => {
      questionRequest()
        .then(questionTransform)
        .then((result) => {
          setIsLoading(false);
          //console.log("Results are:", result);
          setQuestions(result);
        })
        .catch((err) => {
          setIsLoading(false);
          setError(err);
        });
    }, 2000);
  };

  useEffect(() => {
    retrieveQuestions();
  }, []);
  console.log(questions);
  return (
    <QuestionContext.Provider value={{ questions, isLoading, error }}>
      {children}
    </QuestionContext.Provider>
  );
};
