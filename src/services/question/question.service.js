import { mocks } from "./mock";
import camelize from "camelize";
export const questionRequest = (question = 1) => {
  return new Promise((resolve, reject) => {
    const mock = mocks[question];
    if (!mock) {
      reject("Not Found");
    }
    resolve(mock);
  });
};

export const questionTransform = (result) => {
  //console.log("Found Item:", result);
  const mappedResults = {
    id: result.id,
    qnsNo: result.questionNo,
    qnsNoTotal: 10,
    qnsContent: result.questionWord,
    qnsAnswer: result.answer,
  };

  return camelize(mappedResults);
};
