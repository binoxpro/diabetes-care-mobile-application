import questionone from "./questionone.json";
import questiontwo from "./questiontwo.json";
import questionthree from "./questionthree.json";
import questionfour from "./questionfour.json";
import questionfive from "./questionfive.json";
import questionsix from "./questionsix.json";

export const mocks = {
  1: questionone,
  2: questiontwo,
  3: questionthree,
  4: questionfour,
  5: questionfive,
  6: questionsix,
};
