import React, { useState, createContext, useEffect } from "react";

import * as SecureStore from "expo-secure-store";

import {
  loginRequest,
  signUpUser,
  userVerification,
} from "./authentication.service";

//import { save, getValueFor } from "../../utils/preference";

export const AuthenticationContext = createContext();

export const AuthienticationContextProvider = ({ children }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [userId, setUserId] = useState(null);
  const [error, setError] = useState(null);
  const [isVerified, setIsVerified] = useState(false);
  const [isLogin, setIsLogin] = useState(false);
  const [auth, setAuth] = useState(null);

  const checkAuthStatus = async () => {
    //await SecureStore.deleteItemAsync("userId");
    //await SecureStore.deleteItemAsync("verified");
    console.log("Authentication called for user ", userId);
    let key = "userId";
    let result = await SecureStore.getItemAsync(key);
    let result2 = await SecureStore.getItemAsync("verified");
    console.log(
      "Authentication called for Keys are userId:",
      result,
      "Screen",
      result2
    );
    if (result && result2) {
      // let key = "userId";
      // let result = await SecureStore.getItemAsync(key);
      // let result2 = await SecureStore.getItemAsync("verified");
      console.log("Authentication called for Keys are ", result, result2);
      if (result && result2) {
        //console.log(result);
        setUserId(result);
        if (result2 === "true") {
          setAuth("Success");
          setIsVerified(true);
        } else {
          setAuth("Login");
          setError("Please login again");
          setIsVerified(false);
        }
      } else {
        setAuth("Login");
        //setUserId(null);
        //setIsLogin(false);
        setIsVerified(false);
      }
    } else {
      setAuth("Login");
      //setUserId(null);
      setIsLogin(false);
      setIsVerified(false);
    }
    //console.log("Screen val", auth);
  };

  //checkAuthStatus();
  const onLogin = (email, password) => {
    setIsLoading(true);
    loginRequest(email, password)
      .then(async (res) => {
        setIsLoading(false);
        //await SecureStore.setItemAsync("userId", res.userId);
        console.log("Status value:", res.status);
        //let x = res.status === "Authenticated" ? "verified" : res.status;
        //await SecureStore.setItemAsync("verified", res.status);

        if (res.status) {
          let status = res.status ? "true" : "false";
          await SecureStore.setItemAsync("userId", res.userId);
          await SecureStore.setItemAsync("verified", status);
          console.log(
            "Seaver Returned Data ",
            res.status,
            " Converted String ",
            status
          );

          //setError();
        } else {
          setError("Invaild username and password please try again");
        }
        setAuth("check");

        //await checkAuthStatus();
      })
      .catch((err) => {
        setIsLoading(false);
        setError(err);
        setAuth("check");
      });
  };
  const onLogout = () => {};

  const onSignUp = (firstName, lastName, mobileNumber, dob, password) => {
    setIsLoading(true);
    if (firstName && lastName && mobileNumber && dob && password) {
      signUpUser(firstName, lastName, mobileNumber, dob, password)
        .then(async (data) => {
          setIsLoading(false);
          setAuth("Check");
        })
        .catch((error) => {
          setIsLoading(false);
          setIsVerified(false);
          setError(error);
        });
    } else {
      setIsLoading(false);
      setError("Please supply all field !");
    }
  };

  const onVerification = (verificationCode, password) => {
    setIsLoading(true);
    userVerification(userId, verificationCode, password)
      .then(async (data) => {
        setIsLoading(false);

        await SecureStore.setItemAsync("verified", data.status);
        setUserId(userId);
        setAuth("Check");
        //await checkAuthStatus();
      })
      .catch((error) => {
        setIsLoading(false);
        setIsVerified(false);
        setError(error);
      });
  };

  // useEffect(() => {
  //   if (isLogin) {
  //     //setIsLogin(isLogin);
  //     //setIsVerified(isVerified);
  //   }
  // }, [isLogin, isVerified]);

  useEffect(() => {
    if (auth === "check" || !auth) {
      console.log("Checking User effect for user ", userId);
      checkAuthStatus();
    }
  }, [auth]);

  return (
    <AuthenticationContext.Provider
      value={{
        auth,
        isLogin,
        isVerified,
        userId,
        isLoading,
        error,
        onLogin,
        onVerification,
        onSignUp,
      }}
    >
      {children}
    </AuthenticationContext.Provider>
  );
};
