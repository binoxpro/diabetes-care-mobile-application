import { mconfig } from "../../utils/utils";

export const loginRequest = async (mobileNumber, password) => {
  return fetch(mconfig.apiUrl + "users/login", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      username: mobileNumber,
      password: password,
    }),
  }).then((res) => {
    //console.log(res.json())
    return res.json();
  });
};

export const signUpUser = async (firstName,lastName,mobileNumber,dob,password) => {
  return fetch(mconfig.apiUrl + "users/register/", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      firstName:firstName,
      lastName:lastName,
      dob:dob,
      mobileNumber:mobileNumber,
      pword:password
    }),
  }).then((res) => {
    return res.json();
  });
};


export const userVerification = (userId, verificationCode, password) => {
  return fetch(mconfig.apiUrl + "mAuthentication/userVerification", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      userId: userId,
      code: verificationCode,
      password: password,
    }),
  }).then((response) => {
    return response.json();
  });
};
