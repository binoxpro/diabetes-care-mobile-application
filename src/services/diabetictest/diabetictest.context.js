import React, {useState,createContext, useEffect,useContext} from "react";
import {diabeticTestRequest,diabeticTestRequestSave} from "./diabetictest.service";
import {AuthenticationContext}from "../authentication/authentication.context"

export const DiabeticTestContext = createContext();
export const DiabeticTestContextProvider = ({children}) =>{
    const [diabeticTests, setDiabeticTests] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [error,setError] = useState(false);
    const [saveMsg,setSaveMsg] = useState(null);
    const [refreshing, setRefreshing] = useState(true);

    const {userId} = useContext(AuthenticationContext);
    const retrieveDiabeticTest = async () =>{
        setIsLoading(true);
        setDiabeticTests([]);
        diabeticTestRequest().then(async(data)=>{
            setIsLoading(false);
            setRefreshing(false);
            console.log("Return:",data['value']);
            setDiabeticTests(data['value']);
        }).catch((err)=>{
            setIsLoading(false);
            setError(err);
        })
    }

    const saveResult = async(testId,unitId,unitValue) =>{
        setIsLoading(true);
        diabeticTestRequestSave(testId,unitId,unitValue,userId).then(async(data)=>{
            setIsLoading(false);
            setSaveMsg(data['value']);
            console.log("Return:",data['value']);
        }).catch((err)=>{
            setIsLoading(false);
            setError(err);
            setSaveMsg("Something went wrong please try again.");
        });
    }
    useEffect(()=>{
        if(userId&&refreshing)
        {
            console.log("Userid is",userId);
            retrieveDiabeticTest();
            setRefreshing(false);
        }
        
    },[])


    return(
        <DiabeticTestContext.Provider
        value={{
            diabeticTests,
            isLoading,
            error,
            refreshing,
            saveResult,
        }}
        >
            {children}
        </DiabeticTestContext.Provider>
    )
}