import {host} from "../../utils/env";

export const diabeticTestRequest =()=>{
    return fetch(`${host}diabeticTest/getAll`,{
        method:"GET",
        headers:{
            Accept: "application/json",
      "Content-Type": "application/json",
        },
    
    }).then((res)=>{
        return res.json();
    })
}

export const diabeticTestRequestSave=(testId,unitId,unitValue,userId)=>{
    return fetch(`${host}diabeticTest/diabeticUserTestResult/add/`,{
        method:"POST",
        headers:{
            Accept: "*/*",
            "Content-Type": "application/json",
        },
        body:JSON.stringify({
            testId:testId,
            unitId:unitId,
            userId:userId,
            unitValue:unitValue,
        })
    }).then((response)=>{
        return response.json();
    })

}