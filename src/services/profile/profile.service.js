import { host } from "../../utils/env";
export const profileRequest = async (userId) => {
  console.log("The real getting Id", userId);
  return fetch(`${host}users/getProfile`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      id: userId,
    }),
  }).then((res) => {
    return res.json();
  });
};

export const AddProfile = async (profile) => {
  console.log("The real getting Id", profile.userId, "The Obj:", profile);
  return fetch(`${host}users/profile/add/`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      profile: "medical",
      userId: profile.userId,
      weight: profile.weight,
      height: profile.height,
      fvil: profile.fvil,
      ancestor: profile.ancestor,
      type1Diabetes: profile.type1Diabetes,
      type2Diabetes: profile.type2Diabetes,
    }),
  }).then((res) => {
    return res.json();
  });
};

export const getRiskFactor = async (userId) => {
  return fetch(`${host}users/profile/getRisk/`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      userId: userId,
    }),
  }).then((res) => {
    return res.json();
  });
};

// firstName:profile.firstName,
// lastName:profile.lastName,
// dob: profile.dob,
// mobileNumber:profile.mobileNumber,
