import React, { useState, createContext, useEffect, useContext } from "react";
import { profileRequest, AddProfile, getRiskFactor } from "./profile.service";
import { AuthenticationContext } from "../authentication/authentication.context";

export const ProfileContext = createContext();

export const ProfileContextProvider = ({ children }) => {
  const [profile, setProfile] = useState(null);
  const [riskFactor, setRiskFactor] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  //const [profileRequestDone,setProfileRequestDone] = useState(false);
  console.log("Loading again");
  const { userId } = useContext(AuthenticationContext);

  const saveProfile = (profileArr) => {
    console.log("Add Profile Information");
    profileArr.userId = userId;
    setIsLoading(true);
    AddProfile(profileArr)
      .then(async (data) => {
        setIsLoading(false);
        console.log("logging");
      })
      .catch((err) => {
        setIsLoading(false);
        setError(err);
        console.error("Error ", err);
      });
  };
  const retrieveProfile = (userId) => {
    console.log("Retrieving the Profile");
    setIsLoading(true);
    console.log("While retrieving the user Id:", userId);
    profileRequest(userId)
      .then(async (data) => {
        console.log("Object Returned:", data);
        setIsLoading(false);
        setProfile(data);
      })
      .catch((err) => {
        console.error("Error occuried ", err);
        setIsLoading(false);
        setError(err);
      });
  };

  const riskValue = (userId) => {
    setIsLoading(true);
    getRiskFactor(userId)
      .then(async (data) => {
        setIsLoading(false);
        setRiskFactor(data["value"]["risk"]);
        console.log("Risk Factor", data["value"]["risk"]);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  useEffect(() => {
    if (userId) {
      //setProfileRequestDone(true);
      if (!profile) {
        console.log("Getting the User Id :", userId);
        retrieveProfile(userId);
        riskValue(userId);
      }
    }
  }, []);

  return (
    <ProfileContext.Provider
      value={{
        isLoading,
        error,
        profile,
        riskFactor,
        saveProfile,
      }}
    >
      {children}
    </ProfileContext.Provider>
  );
};
