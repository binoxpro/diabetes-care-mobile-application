import React, { useState, createContext, useEffect, useContext } from "react";
import * as SecureStore from "expo-secure-store";

import { startTrip } from "./airwaybill.service";
import { AuthenticationContext } from "../authentication/authentication.context";
export const CompleteAWContext = createContext();

export const CompleteAWContextProvider = ({ children }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const { userId } = useContext(AuthenticationContext);

  const onCompleteReceipt = async (id, np, vp, ap, ts) => {
    setIsLoading(true);

    //console.log(ts);
    startTrip(id, np, vp, ap, userId, ts)
      .then(async (data) => {
        setIsLoading(false);
      })
      .catch((err) => {
        setIsLoading(false);
        setError(err);
      });
  };

  return (
    <CompleteAWContext.Provider
      value={{
        isLoading,
        error,
        onCompleteReceipt,
      }}
    >
      {children}
    </CompleteAWContext.Provider>
  );
};
