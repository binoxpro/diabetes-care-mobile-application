import React, { useState, createContext, useEffect, useContext } from "react";
import * as SecureStore from "expo-secure-store";

import {
  airwayBillRequest,
  startTrip,
  requestTripStart,
  positionDriver,
} from "./airwaybill.service";
import { AuthenticationContext } from "../authentication/authentication.context";

export const AirwayBillContext = createContext();

export const AirwayBillContextProvider = ({ children }) => {
  const [airwaybills, setAirwaybills] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);
  const [refreshing, setRefreshing] = useState(true);

  const { userId } = useContext(AuthenticationContext);

  const retrieveAirwaybills = async (userId, status) => {
    setIsLoading(true);
    //setRefreshing(true);
    setAirwaybills([]);
    airwayBillRequest(userId, status)
      .then(async (data) => {
        setIsLoading(false);
        setRefreshing(false);
        console.log("Returned Value", data);
        // = [];
        //let list = Object.keys(data).map((key) => [key, data[key]]);

        setAirwaybills(data);
      })
      .catch((err) => {
        setIsLoading(false);
        setRefreshing(false);
        setAirwaybills([]);
        setError(err);
      });
  };

  const onCompleteReceipt = async (id, np, vp, ap, ts, fn) => {
    setIsLoading(true);
    startTrip(id, np, vp, ap, userId, ts, fn)
      .then(async (data) => {
        setIsLoading(false);
        console.log(data);
      })
      .catch((err) => {
        setIsLoading(false);
        setError(err);
      });
  };

  const onRequestStartTrip = async (id) => {
    setIsLoading(true);
    requestTripStart(id, userId)
      .then(async (data) => {
        setIsLoading(false);
      })
      .catch((err) => {
        setIsLoading(false);
        setError(err);
      });
  };

  const onTracking = async (lat, lng) => {
    setIsLoading(true);
    positionDriver(lat, lng, userId)
      .then(async (data) => {
        setIsLoading(false);
      })
      .catch((err) => {
        setIsLoading(false);
        setError(err);
      });
  };
  const onRefreshing = async () => {
    retrieveAirwaybills(userId, "NotClosed");
  };

  const getHistoryList = async () => {
    retrieveAirwaybills(userId, "Closed");
  };
  useEffect(() => {
    if (userId) {
      const driverId = userId;
      //console.log(userId);
      retrieveAirwaybills(driverId, "NotClosed");
      //console.log("Data", airwaybills);
    }
  }, [userId]);

  return (
    <AirwayBillContext.Provider
      value={{
        airwaybills,
        isLoading,
        error,
        refreshing,
        onCompleteReceipt,
        onRequestStartTrip,
        onTracking,
        onRefreshing,
        getHistoryList,
      }}
    >
      {children}
    </AirwayBillContext.Provider>
  );
};
