import { host } from "../../utils/env";
export const airwayBillRequest = (userId, status) => {
  return fetch(`${host}airWaybills/list/`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      userId: userId,
      status: status,
    }),
  }).then((res) => {
    return res.json();
  });
};

export const startTrip = (
  id,
  noofpieces,
  volofpiece,
  actualweight,
  userId,
  textSignature,
  formName
) => {
  return fetch(`${host}airWaybills/completeReceipt/`, {
    method: "POST",
    headers: {
      Accept: "*/*",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      airwaybillId: id,
      noOfPieces: noofpieces,
      volWeight: volofpiece,
      actualWeight: actualweight,
      userId: userId,
      textSignature: textSignature,
      formName: formName,
    }),
  }).then((response) => {
    return response.text();
  });
};

export const requestTripStart = (id, userId) => {
  return fetch(`${host}airWaybills/starttrip/`, {
    method: "POST",
    headers: {
      Accept: "*/*",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      airwaybillId: id,
      userId: userId,
    }),
  }).then((response) => {
    return response.text();
  });
};

export const positionDriver = (lat, lng, userId) => {
  return fetch(`${host}airWaybills/positionDriver/`, {
    method: "POST",
    headers: {
      Accept: "*/*",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      driverId: userId,
      lat: lat,
      lng: lng,
    }),
  }).then((response) => {
    return response.text();
  });
};

// export const airwaybillTransform = ({ results = [] }) => {
//   const mappedResults = results.map((airwaybill) => {
//     return {
//       ...airwaybill,
//     };
//   });
//   return mappedResults;
// };
