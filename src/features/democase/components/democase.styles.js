import { TextInput, Title, Button, List } from "react-native-paper";
import { View, Image, SafeAreaView, StatusBar } from "react-native";
import styled from "styled-components/native";

export const SafeArea = styled(SafeAreaView)`
  ${StatusBar.curentHeight && `margin-top:${StatusBar.curentHeight}px`}px;
  margin-top: 15px;
`;
export const Container = styled.View`
  display: flex;
  padding-left: 5px;
  padding-right: 5px;
`;
export const HeaderContainer = styled.View`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const LogoImage = styled.Image`
  width: ${(props) => props.theme.sizes[3]};
  height: ${(props) => props.theme.sizes[3]};
  border-radius: ${(props) => props.theme.sizes[0]};
  background-color: ${(props) => props.theme.colors.bg.primary};
  padding: 25px;
`;
export const Frm = styled.View`
  display: flex;
  flex-direction: column;
  margin-left: ${(props) => props.theme.space[3]};
  padding: ${(props) => props.theme.space[4]};
  border-radius: ${(props) => props.theme.sizes[0]};
`;
export const FrmAction = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  padding: ${(props) => props.theme.space[2]};
`;

export const FooterContainer = styled.View`
  margin-top: 15px;
  align-items: center;
  justify-content: center;
`;

export const Btn = styled(Button)`
  flex: 1;
  height: 50px;
  background-color: #2b60de;
  padding-top: 5px;
`;
