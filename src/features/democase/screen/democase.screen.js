import React from "react";
import {
  SafeArea,
  Container,
  Btn,
  FooterContainer,
  Frm,
  FrmAction,
} from "../components/democase.styles";
import { Text } from "../../../components/typography/text.component";

export const DemoCaseScreen = ({ navigation }) => {
  return (
    <>
      <SafeArea>
        <Container>
          <Frm>
            <Text label="label">
              Would You like to make an Enquiry about the applicablitity of your
              Case with the EACJ ?
            </Text>
            <FrmAction>
              <Btn
                mode="contained"
                onPress={() =>
                  navigation.navigate("InquireDetail", {
                    questionId: 1,
                    status: "new",
                  })
                }
              >
                Start
              </Btn>
            </FrmAction>
          </Frm>
        </Container>
      </SafeArea>
    </>
  );
};
