import React, { useState } from "react";
import { View, Text, StyleSheet, FlatList } from "react-native";
import { Divider } from "react-native-paper";
import { RadioButtonAns } from "../../components/RadioButtonAns";

const renderItem = ({ item }) => {
  return (
    <RadioButtonAns
      itemName={item["answer"]}
      onclick={() => {
        console.log("Clicked");
      }}
    />
  );
};

const key = ({ item }) => {
  return item.id;
};

export const Answer = (answers) => {
  //answers = JSON.stringify(answers);
  return (
    <FlatList
      data={answers}
      renderItem={renderItem}
      keyExtractor={(item) => item["id"].toString()}
    />
  );
};

/*
answers.forEach((item) => {
    console.log("Answer is " + item["answer"] + "-" + item["id"]);
    jsx.push(
      <RadioButtonAns
        itemName={item["answer"]}
        index={item["id"]}
        onclick={() => {
          console.log("Clicked");
        }}
      />
    );
  });
  return jsx;
*/
/*

return (
    <FlatList
      data={JSON.parse(answers)}
      keyExtractor={({ id }, index) => id}
      renderItem={({ item }, index) => (
        <RadioButtonAns
          itemName={item["answer"]}
          index={item["id"]}
          onclick={() => {
            console.log("Clicked");
          }}
        />
      )}
    />
  );*/
