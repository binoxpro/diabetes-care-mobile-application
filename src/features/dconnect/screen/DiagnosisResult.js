import react, { useState, useEffect } from "react";
import { Divider } from "react-native-paper";
import { host } from "../../../utils/env";
import {
  Container,
  ContainerScroll,
  GetStartedBtn,
} from "../components/DiagnosisResult.style";
import { Text } from "../../../components/typography/text.component";
import { Spacer } from "../../../components/spacer/spacer.component";

export const DiagnosisResultScreen = ({ navigation, route }) => {
  const { userId, sessionId } = route.params;
  console.log(userId, sessionId);
  const [medicine, setMedicine] = useState();
  const [diet, setDiet] = useState();
  useEffect(() => {
    if (!medicine && !diet) getData();
  }, []);

  //   const medicine = [
  //     {
  //       medicine: "Parometer",
  //       days: 3,
  //       ttimes: 2,
  //       tabs: 1,
  //       id: 1,
  //       treatmentSessionId: 3,
  //     },
  //     {
  //       medicine: "Parometer 1",
  //       days: 3,
  //       ttimes: 2,
  //       tabs: 1,
  //       id: 2,
  //       treatmentSessionId: 3,
  //     },
  //     {
  //       medicine: "Parometer 2",
  //       days: 3,
  //       ttimes: 2,
  //       tabs: 1,
  //       id: 3,
  //       treatmentSessionId: 3,
  //     },
  //     {
  //       medicine: "Parometer 3",
  //       days: 3,
  //       ttimes: 2,
  //       tabs: 1,
  //       id: 4,
  //       treatmentSessionId: 3,
  //     },
  //   ];

  //   const diet = [{ id: 1, dietName: "Low Fats Meal One" }];

  const List = (med) => {
    //console.log(medicine);
    return med.map((row) => (
      <>
        <Spacer position="top" size="medium" key={row["id"] + "x"} />
        <Text variant="lists" key={row["id"]}>
          {row["medicine"]} : {row["tabs"]} X {row["ttimes"]}: {row["days"]}
          Days
        </Text>
      </>
    ));
  };

  const ListDiet = (dt) => {
    return dt.map((row) => (
      <>
        <Spacer position="top" size="medium" key={row["id"] + "c"} />
        <Text variant="lists" key={row["id"]}>
          {row["dietName"]}
          Days
        </Text>
      </>
    ));
  };

  const getData = async () => {
    let data = await fetch(
      `${host}api/treatmentSession/getDiagnosisData/${userId}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      }
    ).then((response) => {
      return response.json();

      //setDoctors[response.json().value];
    });

    console.log("Data", data);
    setDiet(data["diet"]);
    setMedicine(data["medicine"]);
  };

  const closeSession = async () => {
    const response = await fetch(`${host}api/treatmentSession/close/`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        patientId: userId,
      }),
    }).then((res) => {
      return res.json();
    });
    if (response) {
      console.log(response);
      navigation.navigate("YourDoctors");
    }
  };

  return (
    <Container>
      <Text variant="title">Diagnosis Report</Text>
      <Divider />
      <Text variant="title">Medicine:</Text>
      <Divider />
      {medicine && List(medicine)}
      <Text variant="title">Diet:</Text>
      <Divider />
      {diet && ListDiet(diet)}
      <Divider />
      <GetStartedBtn
        mode="container"
        color="white"
        onPress={() => {
          closeSession();
        }}
      >
        Close Session
      </GetStartedBtn>
    </Container>
  );
};
