import React, { useState, useEffect, useContext } from "react";
import { TouchableOpacity, RefreshControl } from "react-native";
import { host } from "../../../utils/env";

import { ConnectInfoCard } from "../components/Connect-info-card";
import {
  DoctorList,
  DoctorSearchBar,
  LoadContainer,
  Loader,
} from "../components/ConnectWithDoctor.style";

import { AuthenticationContext } from "../../../services/authentication/authentication.context";
export const ConnectWithDoctorScreen = () => {
  const [doctors, setDoctors] = useState([]);
  const [refresh, setRefresh] = useState(false);
  const [refreshing, setRefreshing] = useState(true);
  const { userId } = useContext(AuthenticationContext);
  const getDoctor = async () => {
    let data = await fetch(`${host}api/dconnect/viewall/${userId}`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    }).then((response) => {
      return response.json();

      //setDoctors[response.json().value];
    });

    setRefreshing(false);
    setDoctors(data["value"]);
  };

  useEffect(() => {
    getDoctor();
    // .then((data) => {
    //   setDoctors(data["value"]);
    // })
    // .catch((err) => {
    //   console.log(err);
    // });
  }, []);
  useEffect(() => {
    if (refresh) {
      console.log("Refreshing Now");
      getDoctor();
      setRefresh(false);
    }
  }, [refresh]);

  //   const doctorDataList = [
  //     {
  //       id: 1,
  //       title: "Mr/Ms",
  //       fullName: "James Yiga",
  //       status: "NEW",
  //       aboutUser:
  //         "    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
  //     },
  //     {
  //       id: 2,
  //       title: "Nurse",
  //       fullName: "Okelle James",
  //       status: "AVAIALBE",
  //       aboutUser:
  //         "    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
  //     },
  //     {
  //       id: 3,
  //       title: "Dr",
  //       fullName: "Hellen Akello",
  //       status: "REJECTED",
  //       aboutUser:
  //         "    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
  //     },
  //   ];
  return (
    <>
      <DoctorSearchBar placeholder="Search here " />
      <DoctorList
        data={doctors}
        renderItem={({ item }) => {
          return (
            <ConnectInfoCard
              connectionInfo={item}
              patient={userId}
              refresher={setRefresh}
            />
          );
        }}
        keyExtractor={(item) => item.id.toString()}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={() => {
              getDoctor();
            }}
          />
        }
      />
    </>
  );
};
