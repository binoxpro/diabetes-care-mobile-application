import React from "react";
import {
  Container,
  GetStartedBtn,
} from "../components/ConnectWithScreen.style";
import { Text } from "../../../components/typography/text.component";
import { Spacer } from "../../../components/spacer/spacer.component";

export const ConnectWithScreen = () => {
  return (
    <>
      <Container>
        <Text variant="heading">
          With thousand of Doctor around your Location you can now connect with
          a Doctor of your choice.
        </Text>
        <Spacer position="top" size="large" />
        <GetStartedBtn mode="contained">Connect Now ...</GetStartedBtn>
      </Container>
    </>
  );
};
