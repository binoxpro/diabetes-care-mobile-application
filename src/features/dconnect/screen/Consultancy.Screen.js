import React, { useState, useEffect, useContext } from "react";
import { host } from "../../../utils/env";
import { TouchableOpacity, RefreshControl } from "react-native";
import { SessionInfoCard } from "../components/Session-info-card";
import {
  DoctorList,
  DoctorSearchBar,
  LoadContainer,
  Loader,
} from "../components/ConnectWithDoctor.style";
import { DietContext } from "../../../services/diet/diet.context";
import { AuthenticationContext } from "../../../services/authentication/authentication.context";
export const ConsultancyScreen = ({ navigation }) => {
  const [doctors, setDoctors] = useState([]);
  const [refresh, setRefresh] = useState(false);
  const { userId } = useContext(AuthenticationContext);
  //const { refreshData, checkRefresh } = useContext(DietContext);
  const getDoctor = async () => {
    //checkRefresh();
    //if (refreshData) {
    let data = await fetch(`${host}api/dconnect/viewall/mydoctor/${userId}`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    }).then((response) => {
      return response.json();

      //setDoctors[response.json().value];
    });

    console.log("Doctor and Porcess Id:", data["value"]);
    setDoctors(data["value"]);
    //}
  };
  useEffect(() => {
    getDoctor();
    // .then((data) => {
    //   setDoctors(data["value"]);
    // })
    // .catch((err) => {
    //   console.log(err);
    // });
  }, []);
  const startSessionNow = ({ item }) => {
    console.log("Data");
    console.log(item);
    if (item.Process === "START") {
      navigation.navigate("CaptureDetails", {
        connectInfo: item,
        userId: userId,
      });
    } else if (item.Process === "NEW") {
      console.log("Waiting for reply");
    } else if (item.Process === "TestRequest") {
      navigation.navigate("CaptureList", { sessionInfo: item, userId: userId });
    } else if (item.Process === "DiagnosisResult") {
      navigation.navigate("DiagnosisResult", {
        userId: userId,
        sessionId: item.id,
      });
    } else {
    }
  };

  useEffect(() => {
    if (refresh) {
      console.log("Refreshing Now");
      getDoctor();
      setRefresh(false);
    }
  }, [refresh]);

  //   const doctorDataList = [
  //     {
  //       id: 1,
  //       title: "Mr/Ms",
  //       fullName: "James Yiga",
  //       status: "NEW",
  //       aboutUser:
  //         "    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
  //     },
  //     {
  //       id: 2,
  //       title: "Nurse",
  //       fullName: "Okelle James",
  //       status: "AVAIALBE",
  //       aboutUser:
  //         "    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
  //     },
  //     {
  //       id: 3,
  //       title: "Dr",
  //       fullName: "Hellen Akello",
  //       status: "REJECTED",
  //       aboutUser:
  //         "    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
  //     },
  //   ];
  return (
    <>
      <DoctorSearchBar placeholder="Search here Doctor " />
      <DoctorList
        data={doctors}
        renderItem={({ item }) => {
          return (
            <TouchableOpacity
              onPress={() => {
                startSessionNow({ item });
              }}
            >
              <SessionInfoCard
                connectionInfo={item}
                patient={userId}
                refresher={setRefresh}
              />
            </TouchableOpacity>
          );
        }}
        keyExtractor={(item) => item.id.toString()}
        refreshControl={
          <RefreshControl refreshing={refresh} onRefresh={getDoctor} />
        }
      />
    </>
  );
};
