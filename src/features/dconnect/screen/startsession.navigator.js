import React from "react";
import {
  createStackNavigator,
  TransitionPresets,
} from "@react-navigation/stack";
import { ConsultancyScreen } from "./Consultancy.Screen";
import { StartSessionFrmScreen } from "./StartSessionFrm.Screen";
import { CaptureTestRequestScreen } from "./CaptureTestReqest";
import { DiagnosisResultScreen } from "./DiagnosisResult";
import { DietContextProvider } from "../../../services/diet/diet.context";

const StartSessionStack = createStackNavigator();

export const StartSessionNavigator = () => {
  return (
    <DietContextProvider>
      <StartSessionStack.Navigator headerShown="false">
        <StartSessionStack.Screen
          name="YourDoctors"
          component={ConsultancyScreen}
          options={{
            headerShown: false,
          }}
        />
        <StartSessionStack.Screen
          name="CaptureDetails"
          component={StartSessionFrmScreen}
          options={{
            headerShown: false,
          }}
        />
        <StartSessionStack.Screen
          name="CaptureList"
          component={CaptureTestRequestScreen}
          options={{
            headerShown: false,
          }}
        />
        <StartSessionStack.Screen
          name="DiagnosisResult"
          component={DiagnosisResultScreen}
          options={{
            headerShown: false,
          }}
        />
      </StartSessionStack.Navigator>
    </DietContextProvider>
  );
};
