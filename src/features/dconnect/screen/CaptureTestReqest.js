import React, { useState, useEffect } from "react";
import { Text } from "../../../components/typography/text.component";
import {
  TestList,
  HeadingText,
  GetStartedBtn,
} from "../components/Capture-test-card.style";
import { CaptureTestListScreen } from "../components/CaptureTest-List.Screen";
import { host } from "../../../utils/env";
export const CaptureTestRequestScreen = ({ navigation, route }) => {
  const { sessionInfo, userId } = route.params;
  const [requestedTest, setRequestedTest] = useState();

  useEffect(() => {
    if (!requestedTest) {
      console.log("The Item ID :", sessionInfo.userId);
      getTestInfo(userId, sessionInfo.userId);
      console.log("Loading is ");
    }
  }, []);

  const getTestInfo = async (userId, id) => {
    let data = await fetch(
      `${host}api/treatmenttestrequest/view/${userId}/${id}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      }
    ).then((response) => {
      return response.json();

      //setDoctors[response.json().value];
    });

    setRequestedTest(data);
  };

  const completedTesting = () => {
    navigation.navigate("YourDoctors");
  };

  return (
    <>
      <HeadingText>
        
        <Text variant="heading">
          Do the Follow Test and Record value to the Test
        </Text>
        <GetStartedBtn
          mode="contained"
          onPress={() => {
            completedTesting();
          }}
        >
          Done
        </GetStartedBtn>
      </HeadingText>

      <TestList
        data={requestedTest}
        renderItem={({ item }) => {
          return <CaptureTestListScreen requestTest={item} />;
        }}
        keyExtractor={(item) => item.id.toString()}
      />
    </>
  );
};
