import React, { useState, useEffect, useContext } from "react";
import { FlatList } from "react-native";
import { MD3Colors, Divider, Avatar } from "react-native-paper";
import { Text } from "../../../components/typography/text.component";
import { Spacer } from "../../../components/spacer/spacer.component";
import {
  Container,
  FrmHeading,
  TxtSympton,
  HeadingControl,
  AddBtn,
  ListContainer,
  StartBtn,
} from "../components/StartSession.Style";
import { host } from "../../../utils/env";
import { AuthenticationContext } from "../../../services/authentication/authentication.context";

export const StartSessionFrmScreen = ({ navigation, route }) => {
  //const { userId } = useContext(AuthenticationContext);

  const { connectInfo, userId } = route.params;

  const [patientBrief, setPatientBrief] = useState([]);
  const [txtBrief, setTxtBrief] = useState("");
  const [sessionNumber, setSessionNumber] = useState();
  const [btnText, setBtnText] = useState(false);
  const addBrief = () => {
    let count = patientBrief.length + 1;
    patientBrief.push({ id: count, name: txtBrief });
    setTxtBrief("");
    console.log(patientBrief);
  };

  const getSessionNumber = async () => {
    //get Start
    let x = await fetch(`${host}api/treatmentSession/getSessionNo/`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    }).then((response) => {
      return response.json();
    });
    return x;
  };
  useEffect(() => {
    if (!sessionNumber) {
      getSessionNumber().then((data) => {
        console.log("The Session Data", data);
        setSessionNumber(data["sessionNo"]);
        console.log(data["sessionNo"]);
        console.log(connectInfo, " ", userId);
      });
    }
  }, []);

  const saveBriefs = async () => {
    const response = await fetch(`${host}api/treatmentSession/start/`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id: sessionNumber,
        patientId: userId,
        doctorId: connectInfo.userId,
        briefs: patientBrief,
      }),
    }).then((res) => {
      return res.json();
    });
    if (response) {
      console.log(response);
      navigation.navigate("YourDoctors");
    }
  };

  useEffect(() => {}, [patientBrief]);

  return (
    <>
      <Container>
        <FrmHeading>
          <Text variant="heading">
            Please describe to the doctor your symptons!
          </Text>
        </FrmHeading>
        <HeadingControl>
          <TxtSympton
            value={txtBrief}
            onChangeText={(e) => {
              setTxtBrief(e);
            }}
          />
          <AddBtn
            icon="plus"
            iconColor="white"
            size={40}
            mode="contained"
            onPress={() => {
              addBrief();
            }}
          />
        </HeadingControl>
        <Spacer />
        <Divider />
        <ListContainer>
          <FlatList
            data={patientBrief}
            renderItem={({ item }) => {
              return (
                <>
                  <Text variant="heading">
                    <Avatar.Icon size={20} icon="circle" />
                    {item.name}
                  </Text>
                  <Divider />
                </>
              );
            }}
            keyExtractor={(item) => item.id.toString()}
          />
        </ListContainer>
        <StartBtn
          mode="elevated"
          textColor="white"
          onPress={() => {
            saveBriefs();
          }}
        >
          Start Session Now
        </StartBtn>
      </Container>
    </>
  );
};
