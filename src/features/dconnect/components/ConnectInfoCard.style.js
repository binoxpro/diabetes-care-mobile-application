import { Card, Button } from "react-native-paper";
import { View } from "react-native";
import styled from "styled-components/native";

export const ConnectCard = styled(Card)`
  margin: ${(props) => props.theme.sizes[0]};
  border-left-width: 4px;
  border-left-color: #4caf50;
  display: flex;
`;
export const Container = styled(View)`
  border-width: 0px;
  background-color: #fff;
  padding: 1px;
  flex-direction: row;
`;

export const ImgContainer = styled(View)`
  justifycontent: center;
  alignitems: center;
  display: flex;

  margin-left: 5px;
  margin-right: 5px;
  margin-top: 5px;
`;
export const TxtContainer = styled(View)`
  width: 65%;
  display: flex;
`;
export const Btn = styled(Button)`
  justify-content: center;
  background-color: #067348;
  border-radius: 5px;
  height: 40px;
  padding: 1px;
  width: 100%;
  margin-right: 2.5%;
`;
export const BtnLft = styled(Button)`
  justify-content: center;
  background-color: #d30d28;
  border-radius: 5px;
  height: 40px;
  padding: 1px;
  width: 48%;
  margin-left: 2.5%;
`;
export const BtnArea = styled(Card.Actions)`
  display: flex;
  width: 100%;
  flex-direction: row;
  box-shadow: 2px 1px #90908f;
`;
export const BtnConnect = styled(Button)`
  justify-content: center;
  background-color: #067348;
  border-radius: 5px;
  height: 40px;
  padding: 1px;
  width: 40%;
  margin-right: 2.5%;
`;
export const BtnWaiting = styled(Button)`
  justify-content: center;
  background-color: #e67e22;
  border-radius: 5px;
  height: 40px;
  padding: 1px;
  width: 40%;
  margin-right: 2.5%;
`;
export const BtnConnected = styled(Button)`
  justify-content: center;
  background-color: #34495e;
  border-radius: 5px;
  height: 40px;
  padding: 1px;
  width: 40%;
  margin-right: 2.5%;
`;
