import { Card, Button, TextInput } from "react-native-paper";
import { View, FlatList } from "react-native";
import styled from "styled-components/native";

export const HeadingText = styled(View)`
  margin-left: 1%;
  margin-right: 1%;
  display: flex;
  width: 98%;
  padding: 5px;
  padding-bottom: 15px;
  box-shadow: 1px 5px 3px #515a5a;
`;

export const Container = styled(View)`
  display: flex;
  width: 96%;
  background-color: #eaeded;
  padding: ${(props) => props.theme.sizes[2]};
  margin-top: 15px;
  margin-left: 2%;
  margin-right: 2%;
  border: 2px solid #eaeded;
  border-radius: 8px;
  box-shadow: 1px 2px 5px #b2babb;
`;

export const TestVal = styled(TextInput)`
  display: flex;
`;

export const GetStartedBtn = styled(Button)`
  width: 100%;
  height: 50px;
  justify-content: center;
  background-color: #0e6655;
  margin-top: 15px;
`;

export const TestList = styled(FlatList).attrs({
  contentContainerStyle: {
    padding: 16,
  },
})``;
