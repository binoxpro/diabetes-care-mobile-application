import React, { useState } from "react";
import { host } from "../../../utils/env";
import { Divider } from "react-native-paper";
import { Container, GetStartedBtn, TestVal } from "./Capture-test-card.style";
import { Text } from "../../../components/typography/text.component";

export const CaptureTestListScreen = ({ requestTest = {} }) => {
  const [testValue, setTestValue] = useState();
  //console.log("Rea:", requestTest);
  const {
    id = 1,
    treatmentsessionId = 3,
    testId = 1,
    testName = "Oral Gluse Tolerance Test",
    testDescription = "Lorem ipsum dolor sit amet",
  } = requestTest;

  const saveTestListScreen = async (id) => {
    console.log("Capture:", testValue, " id:", id);
    const response = await fetch(`${host}api/treatmenttestrequestvalue/add/`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        treatmenttestrequestId: id,
        value: testValue,
      }),
    }).then((res) => {
      return res.json();
    });
    if (response) {
      console.log(response);
      //navigation.navigate("YourDoctors");
    }
  };

  return (
    <Container>
      <Text variant="heading">{testName}</Text>
      <Text variant="heading">{testDescription}</Text>
      <Divider />
      <TestVal
        label="Test Value (mm/gl)"
        mode="outlined"
        value={testValue}
        onChangeText={(text) => {
          setTestValue(text);
        }}
      />
      <GetStartedBtn
        mode="outlined"
        color="#B2BABB"
        onPress={() => {
          console.log("Pressed");
          saveTestListScreen(id);
        }}
      >
        SAVE
      </GetStartedBtn>
    </Container>
  );
};
