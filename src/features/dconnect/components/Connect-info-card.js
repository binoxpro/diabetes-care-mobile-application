import React from "react";
import { TouchableOpacity } from "react-native";
import {
  ConnectCard,
  Btn,
  BtnLft,
  BtnArea,
  BtnConnect,
  BtnConnected,
  BtnWaiting,
  Container,
  ImgContainer,
  TxtContainer,
} from "./ConnectInfoCard.style";
import { Divider, Avatar } from "react-native-paper";
import { Text } from "../../../components/typography/text.component";
import { host } from "../../../utils/env";

export const ConnectInfoCard = ({
  connectionInfo = {},
  patient = -1,
  refresher,
}) => {
  const {
    id = -1,
    title = "Mr/Ms",
    fullName = "James Yiga",
    aboutUser = "    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    status = "Connect",
    userId = "",
  } = connectionInfo;
  console.log("Testing ", connectionInfo.status);
  const connectNow = async (connectionInfo, patient) => {
    //
    console.log("connection ", connectionInfo, " ", patient);
    let response = await fetch(`${host}api/dconnect/add/`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        patientId: patient,
        doctorId: connectionInfo.userId,
      }),
    }).then((res) => {
      return res.json();
    });
    refresher(true);
    console.log("Log", response);
  };
  return (
    <ConnectCard>
      <ConnectCard.Content>
        <Container>
          <ImgContainer>
            <Avatar.Image
              size={100}
              source={require("../../../../assets/avatar_new.png")}
            />
          </ImgContainer>
          <TxtContainer>
            <Text variant="heading">
              {title},{fullName}
            </Text>
            <Divider />
            <Text variant="caption">{aboutUser}</Text>
          </TxtContainer>
        </Container>
        <BtnArea>
          {status === "Connect" ? (
            <BtnConnect
              mode="contained"
              onPress={() => {
                connectNow(connectionInfo, patient);
              }}
            >
              {status}
            </BtnConnect>
          ) : status == "WAITING" ? (
            <BtnWaiting
              mode="contained"
              onPress={() => {
                connectNow(connectionInfo, patient);
              }}
            >
              {status}
            </BtnWaiting>
          ) : status == "CONsTINUE TO TESTREQUEST" ? (
            <BtnConnected
              mode="contained"
              onPress={() => {
                connectNow(connectionInfo, patient);
              }}
            >
              Please Do Test
            </BtnConnected>
          ) : (
            <BtnConnected
              mode="contained"
              onPress={() => {
                connectNow(connectionInfo, patient);
              }}
            >
              {status}
            </BtnConnected>
          )}

          <BtnLft mode="contained">Disconnect</BtnLft>
        </BtnArea>
      </ConnectCard.Content>
    </ConnectCard>
  );
};
