import { View } from "react-native";
import { TextInput, Button, IconButton } from "react-native-paper";
import styled from "styled-components/native";

export const Container = styled(View)`
  display: flex;
  background-color: #d7dbdd;
  margin-top: 5%;
  margin-left: 2%;
  margin-right: 2%;

  border-radius: 10px;
  height: 95%;
  width: 95%;
  padding: 2px;

  box-shadow: 6px 3px 2px #d7dbdd;
`;

export const FrmHeading = styled(View)`
  align-items: center;
  justify-content: center;
  display: flex;
  margin-top: 15px;
`;
export const HeadingControl = styled(View)`
  display: flex;
  flex-direction: row;
`;
export const TxtSympton = styled(TextInput)`
  margin: 2%;
  width: 80%;
`;
export const AddBtn = styled(IconButton)`
  background-color: #196f3d;
`;
export const StartBtn = styled(Button)`
  font-size: 12px;
  width: 80%;
  background-color: #196f3d;
  height: 50px;
  align-item: center;
  justify-content: center;
  margin-left: 10%;
  color: rgb(0, 0, 255);
`;
export const ListContainer = styled(View)`
  padding: 5%;
`;
