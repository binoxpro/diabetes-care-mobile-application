import { View, ScrollView } from "react-native";
import { Button } from "react-native-paper";
import styled from "styled-components/native";

export const ContainerScroll = styled(ScrollView)`
  display: flex;
  height: 100%;
`;
export const Container = styled(View)`
  display: flex;
  margin-top: 5%;
  margin-left: 4%;
  margin-right: 4%;
  width: 92%;
  height: 92%;
  background-color: #d5dbdb;
  border-radius: 10px;
  box-shadow: 1px 5px 2px #707b7c;
  padding: 15px;
`;
export const Title = styled(View)``;
export const GetStartedBtn = styled(Button)`
  width: 100%;
  height: 50px;
  justify-content: center;
  background-color: #0e6655;
  margin-top: 15px;
`;
