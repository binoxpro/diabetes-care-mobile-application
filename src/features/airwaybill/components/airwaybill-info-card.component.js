import React from "react";
import { Text } from "../../../components/typography/text.component";
import { AirwayCard } from "../../../components/card.component";
import {
  AirwayBillCard,
  BtnCapture,
  InfoContainer,
  OrignContainer,
  DesignationContainer,
  DetailContainer,
  InstructionContainer,
  HeaderContainer,
  HeaderTextArea,
  HeaderBtnArea,
} from "./airwaybillcard.style";

export const AirWayBillInfoCard = ({ airWayBill = {} }) => {
  const {
    captureDate = "E211000001",
    status = "New",
    origin = "Mbale",
    companyName = "Xonib Software Sytems",
    street = "Lumbama Avenu",
    name = "Yiga James",
    phoneNumber = "0781587081",
    destination = "Entebbe",
    companyNameR = "Xonib Software ",
    streetR = "Lumbama Avenu",
    nameR = "Yiga James",
    phoneNumberR = "0781587081",
    instructions = "Keep Frozen",
    contents = "Blood Samples",
    processName = "",
    airwaybillId = "",
    id = "",
    noOfPieces = "",
    volWeight = "",
    actualWeight = "",
  } = airWayBill;
  console.log("Array Val:", airWayBill);
  const getProcess = (proc) => {
    let val = null;
    switch (proc) {
      case "Driver Aknowledged":
        val = "NEW";
        break;
      case "Driver Picks up Shipment":
        val = "PICKED";
        break;
      case "Shipment in Transit":
        val = "TRANSIT";
        break;
      case "Delivered":
        val = "DELIVERED";
        break;
      case "Closed":
        val = "CLOSED";
        break;

      default:
        val = "Not Supported";
        break;
    }
    return val;
  };

  const getCardVariant = (proc) => {
    let val = null;
    switch (proc) {
      case "NEW":
        val = "newcard";
        break;
      case "PICKED":
        val = "pickcard";
        break;
      case "TRANSIT":
        val = "transitcard";
        break;
      case "DELIVERED":
        val = "deliveredcard";
        break;
      case "CLOSED":
        val = "closedcard";
        break;

      default:
        val = "Not Supported";
        break;
    }
    return val;
  };

  const process = getProcess(airWayBill.processName);
  const variantstr = getCardVariant(process);
  //=== "Driver Aknowledged" ? "New" : "";

  return (
    <AirwayCard variant={variantstr}>
      <AirwayBillCard.Content>
        <HeaderContainer>
          <HeaderTextArea>
            <Text variant="numlock">{airwaybillId} </Text>
          </HeaderTextArea>
          <HeaderBtnArea>
            <Text>{process} </Text>
          </HeaderBtnArea>
        </HeaderContainer>
        <InfoContainer>
          <OrignContainer>
            <Text>From: {origin}</Text>
            <Text>{companyName}</Text>
            <Text>{street}</Text>
            <Text>{name}</Text>
            <Text>{phoneNumber}</Text>
          </OrignContainer>
          <DesignationContainer>
            <Text>To: {destination}</Text>
            <Text>{companyNameR}</Text>
            <Text>{streetR}</Text>
            <Text>{nameR}</Text>
            <Text>{phoneNumberR}</Text>
          </DesignationContainer>
        </InfoContainer>
        <InstructionContainer>
          <Text>{instructions}</Text>
        </InstructionContainer>
        <DetailContainer>
          <Text>{contents}</Text>
        </DetailContainer>
      </AirwayBillCard.Content>
    </AirwayCard>
  );
};
