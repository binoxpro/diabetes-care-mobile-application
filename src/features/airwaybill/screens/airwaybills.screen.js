import React, { useState, useContext } from "react";
import { TouchableOpacity, Alert, RefreshControl } from "react-native";
import { AirWayBillInfoCard } from "../components/airwaybill-info-card.component";
import {
  AirwayBillList,
  AirwayBillSearch,
  LoadContainer,
  Loader,
} from "../components/airwaybilllist.style";
import { Text } from "../../../components/typography/text.component";
import { AirwayBillContext } from "../../../services/airwaybill/airwaybill.context";
import styled from "styled-components";

export const AirwayBillsScreen = ({ navigation }) => {
  const [seachQuery, setSearchQuery] = useState();
  //const airwayBills = [];
  const {
    isLoading,
    error,
    refreshing,
    airwaybills,
    onRequestStartTrip,
    onRefreshing,
  } = useContext(AirwayBillContext);
  //console.log("Loadind :", isLoading);
  const hasError = !!error;
  const onChangeSearch = (query) => setSearchQuery(query);
  const startTripAlert = ({ item }) => {
    Alert.alert(
      "Start Confirmation",
      "Would like to start trip for shipment to " +
        item.destination +
        "?" +
        item.airwaybillId,
      [
        { text: "NO", style: "cancel" },
        {
          text: "YES",
          onPress: () => {
            onRequestStartTrip(item.airwaybillId);
          },
        },
      ]
    );
  };

  const getProcess = (proc) => {
    let val = null;
    switch (proc) {
      case "Driver Aknowledged":
        val = "NEW";
        break;
      case "Driver Picks up Shipment":
        val = "PICKED";
        break;
      case "Shipment in Transit":
        val = "TRANSIT";
        break;
      case "Delivered":
        val = "DELIVERED";
        break;

      default:
        val = "Not Supported";
        break;
    }
    return val;
  };
  const getNextView = ({ item }) => {
    let status = getProcess(item.processName);
    if (status === "NEW") {
      navigation.navigate("AirwayBillDetails", {
        airwaybill: item,
        formCaption: "Confirm Receipt",
      });
    } else if (status === "PICKED") {
      startTripAlert({ item });
    } else if (status === "TRANSIT") {
      navigation.navigate("AirwayBillDetails", {
        airwaybill: item,
        formCaption: "Confirm Delivery",
      });
    }
  };
  // const airwayBills = [
  //   {
  //     date: "E211000001",
  //     status: "New",
  //     origin: "Mbale",
  //     companyName: "Xonib Software Sytems",
  //     street: "Lumbama Avenu",
  //     name: "Yiga James",
  //     phoneNumber: "0781587081",
  //     destinate: "Entebbe",
  //     companyNameR: "Xonib Software ",
  //     streetR: "Lumbama Avenu",
  //     nameR: "Yiga James",
  //     phoneNumberR: "0781587081",
  //     instructions: "Dry ICE",
  //     details: "Blood Samples",
  //   },
  //   {
  //     date: "E211000002",
  //     status: "New",
  //     origin: "Mbale",
  //     companyName: "Xonib Software Sytems",
  //     street: "Lumbama Avenu",
  //     name: "Yiga James",
  //     phoneNumber: "0781587081",
  //     destinate: "Entebbe",
  //     companyNameR: "Xonib Software ",
  //     streetR: "Lumbama Avenu",
  //     nameR: "Yiga James",
  //     phoneNumberR: "0781587081",
  //     instructions: "Dry ICE",
  //     details: "Blood Samples",
  //   },
  //   {
  //     date: "E211000003",
  //     status: "New",
  //     origin: "Mbale",
  //     companyName: "Xonib Software Sytems",
  //     street: "Lumbama Avenu",
  //     name: "Yiga James",
  //     phoneNumber: "0781587081",
  //     destinate: "Entebbe",
  //     companyNameR: "Xonib Software ",
  //     streetR: "Lumbama Avenu",
  //     nameR: "Yiga James",
  //     phoneNumberR: "0781587081",
  //     instructions: "Dry ICE",
  //     details: "Blood Samples",
  //   },
  //   {
  //     date: "E211000004",
  //     status: "New",
  //     origin: "Mbale",
  //     companyName: "Xonib Software Sytems",
  //     street: "Lumbama Avenu",
  //     name: "Yiga James",
  //     phoneNumber: "0781587081",
  //     destinate: "Entebbe",
  //     companyNameR: "Xonib Software ",
  //     streetR: "Lumbama Avenu",
  //     nameR: "Yiga James",
  //     phoneNumberR: "0781587081",
  //     instructions: "Dry ICE",
  //     details: "Blood Samples",
  //   },
  //   {
  //     date: "E211000005",
  //     status: "New",
  //     origin: "Mbale",
  //     companyName: "Xonib Software Sytems",
  //     street: "Lumbama Avenu",
  //     name: "Yiga James",
  //     phoneNumber: "0781587081",
  //     destinate: "Entebbe",
  //     companyNameR: "Xonib Software ",
  //     streetR: "Lumbama Avenu",
  //     nameR: "Yiga James",
  //     phoneNumberR: "0781587081",
  //     instructions: "Dry ICE",
  //     details: "Blood Samples",
  //   },
  // ];
  //onRefreshing();
  return (
    <>
      <AirwayBillSearch
        placeholder="Search for Job"
        value={seachQuery}
        onChangeText={onChangeSearch}
      />
      {isLoading && (
        <LoadContainer>
          <Loader animating={true} />
        </LoadContainer>
      )}
      {refreshing ? (
        <LoadContainer>
          <Loader animating={true} />
        </LoadContainer>
      ) : null}
      {airwaybills && (
        <AirwayBillList
          data={airwaybills}
          renderItem={({ item }) => {
            return (
              <TouchableOpacity
                onPress={() => {
                  getNextView({ item });
                }}
              >
                <AirWayBillInfoCard airWayBill={item} />
              </TouchableOpacity>
            );
          }}
          keyExtractor={(item) => item.id.toString()}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefreshing} />
          }
        />
      )}
    </>
  );
};
