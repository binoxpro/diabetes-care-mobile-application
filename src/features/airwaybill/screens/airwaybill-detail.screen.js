import React, { useState } from "react";
import { ScrollView } from "react-native";
import { Divider } from "react-native-paper";
import { AirWayBillInfoCard } from "../components/airwaybill-info-card.component";
import {
  Container,
  Frm,
  TxtInput,
  BtnSm,
  TopContainer,
} from "../components/airwaybilldetail.style";

export const AirwayBillDetailScreen = ({ navigation, route }) => {
  const { airwaybill, formCaption } = route.params;
  console.log("Actual Object: >", formCaption);
  const [noofpiece, setNoofpiece] = useState(airwaybill.noOfPieces.toString());
  const [volofpiece, setVolofpiece] = useState(airwaybill.volWeight.toString());
  const [actualWeight, setActualWeight] = useState(
    airwaybill.actualWeight.toString()
  );

  // setNoofpiece(airwaybill.noOfPieces);
  // setVolofpiece(airwaybill.volWeight);
  // setActualWeight(airwaybill.actualWeight);
  return (
    <Container>
      <TopContainer>
        <AirWayBillInfoCard airWayBill={airwaybill} />
        <Divider />

        <ScrollView>
          <Frm>
            <TxtInput
              label="No Of Pieces"
              value={noofpiece}
              onChangeText={(text) => setNoofpiece(text)}
            />
            <TxtInput
              label="Volume"
              value={volofpiece}
              onChangeText={(text) => setVolofpiece(text)}
            />
            <TxtInput
              label="Actual Weight"
              value={actualWeight}
              onChangeText={(text) => setActualWeight(text)}
            />

            <BtnSm
              onPress={() => {
                navigation.navigate("AirwayBillSignaturePad", {
                  pieces: noofpiece,
                  volume: volofpiece,
                  actual: actualWeight,
                  formName: formCaption,
                  id: airwaybill.airwaybillId,
                });
              }}
            >
              {formCaption}
            </BtnSm>
          </Frm>
        </ScrollView>
      </TopContainer>
    </Container>
  );
};
