import React, { useRef, useState, useContext } from "react";
import { StyleSheet, Image } from "react-native";
import SignatureScreen from "react-native-signature-canvas";
import {
  Container,
  Btn,
  ImageContainer,
  SignImage,
} from "../components/airwaybilldetail.style";

import { Text } from "../../../components/typography/text.component";
import { AirwayBillContext } from "../../../services/airwaybill/airwaybill.context";

export const AirwayBillSignturePadScreen = ({ navigation, route }) => {
  const [signature, setSignature] = useState(null);
  const [np, setNp] = useState(null);
  const [vp, setVp] = useState(null);
  const [ap, setAp] = useState(null);
  const [airwayBillId, setairwayBillId] = useState(null);
  const { isLoading, error, onCompleteReceipt } = useContext(AirwayBillContext);

  //const onCompleteReceipts = (airwayBillId, np, vp, ap, signature) => {};
  const { actual, pieces, volume, id, formName } = route.params;
  console.log(actual, pieces, volume, id);
  // setNp(pieces);
  // setVp(volume);
  // setAp(actual);
  // setairwayBillId(id);
  //setNp(p);;
  const handleSignature = (signature) => {
    //console.log(signature);
    setSignature(signature);
  };

  const handleEmpty = () => {
    console.log("Empty");
  };

  return (
    <Container>
      <SignatureScreen
        onOK={handleSignature}
        onEmpty={handleEmpty}
        autoClear={true}
      />
      <ImageContainer>
        {signature ? (
          <SignImage
            resizeMode={"contain"}
            style={{ width: 335, height: 114 }}
            source={{ uri: signature }}
          />
        ) : null}
      </ImageContainer>
      <Btn
        onPress={() => {
          //console.log(signature);
          onCompleteReceipt(id, pieces, volume, actual, signature, formName);
          navigation.navigate("AirwayBills");
        }}
      >
        <Text>Done</Text>
      </Btn>
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    padding: 10,
  },
  row: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    alignItems: "center",
  },
});
/*
"react-native-signature-canvas": "^1.7.1",
    "react-native-web": "^0.11.7"*/
