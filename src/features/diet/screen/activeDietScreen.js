import React from "react";
import { FlatList } from "react-native";
import { Btn, BtnArea } from "../components/selectDiet.style";
import { ActiveDietInfoCard } from "../components/activeDiet-info.component";

export const ActiveDietScreen = () => {
  const twoDateDiet = [
    {
      day: "Today",
      rule: [
        {
          meal: "Breakfast",
          rules: [
            { id: 1, rule: "Rule 1" },
            { id: 2, rule: "Rule 2" },
            { id: 3, rule: "Rule 3" },
          ],
        },
        {
          meal: "Lunch",
          rules: [
            { id: 1, rule: "Rule 1" },
            { id: 2, rule: "Rule 2" },
            { id: 3, rule: "Rule 3" },
          ],
        },
        {
          meal: "Dinner",
          rules: [
            { id: 1, rule: "Rule 1" },
            { id: 2, rule: "Rule 2" },
            { id: 3, rule: "Rule 3" },
          ],
        },
      ],
    },
    {
      day: "Tomorrow",
      rule: [
        {
          meal: "Breakfast",
          rules: [
            { id: 1, rule: "Add rounded corners" },
            { id: 2, rule: "Rule 2" },
            { id: 3, rule: "Rule 3" },
          ],
        },
        {
          meal: "Lunch",
          rules: [
            { id: 1, rule: "Rule 1" },
            { id: 2, rule: "Rule 2" },
            { id: 3, rule: "Rule 3" },
          ],
        },
        {
          meal: "Dinner",
          rules: [
            { id: 1, rule: "Rule 1" },
            { id: 2, rule: "Rule 2" },
            { id: 3, rule: "Rule 3" },
          ],
        },
      ],
    },
  ];
  return (
    <>
      <BtnArea>
        <Btn mode="contained">Meals</Btn>
      </BtnArea>

      <FlatList
        data={twoDateDiet}
        renderItem={({ item }) => {
          return <ActiveDietInfoCard activeDietInfo={item} />;
        }}
        keyExtractor={(item) => item.day.toString()}
      />
    </>
  );
};
