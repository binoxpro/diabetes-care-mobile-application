import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { SelectDietScreen } from "./selectDiet.Screen";
import { GetStartedScreen } from "./getStarted.screen";
import { DietDayRuleScreen } from "./dietDayRule.Screen";
import { DietCaptureScreen } from "./dietCapture.Screen";
import { DietContextProvider } from "../../../services/diet/diet.context";

const DietStack = createStackNavigator();
export const DietNavigator = () => {
  return (
    <DietContextProvider>
      <DietStack.Navigator>
        <DietStack.Screen name="dietList" component={SelectDietScreen} />
        <DietStack.Screen name="newDiet" component={DietDayRuleScreen} />
        <DietStack.Screen name="captureDiet" component={DietCaptureScreen} />
      </DietStack.Navigator>
    </DietContextProvider>
  );
};
