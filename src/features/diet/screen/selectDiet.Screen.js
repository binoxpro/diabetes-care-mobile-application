import React, { useState, useEffect, useContext } from "react";
import { host } from "../../../utils/env";
import { DietInfoCard } from "../components/diet-info-card.component";
import { FlatList } from "react-native";
import { Searchbar } from "react-native-paper";

import { AuthenticationContext } from "../../../services/authentication/authentication.context";
export const SelectDietScreen = ({ navigation }) => {
  const { userId } = useContext(AuthenticationContext);
  const [diets, setDiets] = useState(null);

  const dateToday = () => {
    let today = new Date();

    let y = today.getFullYear();
    let m = today.getMonth() + 1;
    m = m > 9 ? m : "0" + m;
    let d = today.getDate();
    d = d > 9 ? d : "0" + d;

    return y + "-" + m + "-" + d;
  };
  const [creationDate, setCreationDate] = useState(dateToday());
  const getDiets = async () => {
    let data = await fetch(`${host}api/diet/view/${userId}`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    }).then((response) => {
      return response.json();

      //setDoctors[response.json().value];
    });

    console.log("Doctor and Porcess Id:", data);
    setDiets(data);
  };

  const getPlanFollowUp = async (datav) => {
    let sendData = {
      patientId: userId,
      creationDate: creationDate,
      patientDietId: datav.dietSelectId,
    };
    let data = await fetch(`${host}api/diet/planfollowup`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(sendData),
    }).then((response) => {
      return response.json();

      //setDoctors[response.json().value];
    });

    console.log("Doctor and Porcess Id:", data);
    return data;
    //setDiets(data);
  };

  const pressContent = async (data = null) => {
    //add the params
    console.log("press Content Pressded", data);
    let json = await getPlanFollowUp(data);
    console.log("JSON:", json);
    navigation.navigate("newDiet", {
      data: json,
      dietSelectId: data.dietSelectId,
    });
  };

  useEffect(() => {
    if (!diets) {
      getDiets();
    }
  }, []);

  return (
    <>
      <Searchbar placeholder="Search for Diet Type" />
      <FlatList
        data={diets}
        renderItem={({ item }) => {
          return (
            <DietInfoCard
              dietInfo={item}
              userId={userId}
              clickNow={pressContent}
            />
          );
        }}
        keyExtractor={(item) => item.id.toString()}
      />
    </>
  );
};
