import React, { useState, useEffect } from "react";
import { host } from "../../../utils/env";
import { ScrollView } from "react-native";
import {
  Container,
  InputContainer,
  TextAreaBox,
  FoodText,
  AddBtn,
  BtnArea,
  ListContainer,
} from "../components/dietCapture.Style";
import { Text } from "../../../components/typography/text.component";
import { Spacer } from "../../../components/spacer/spacer.component";
import { Divider } from "react-native-paper";
import { FlatList } from "react-native-gesture-handler";

export const DietCaptureScreen = ({ route }) => {
  const [foodEaten, setFoodEaten] = useState("");
  const [foodList, setFoodList] = useState();
  const [listRefreshed, setListRefreshed] = useState();

  const { day, dietSelectId } = route.params;
  const dateToday = () => {
    let today = new Date();

    let y = today.getFullYear();
    let m = today.getMonth() + 1;
    m = m > 9 ? m : "0" + m;
    let d = today.getDate();
    d = d > 9 ? d : "0" + d;

    return y + "-" + m + "-" + d;
  };

  const textContain = (item) => {
    return (
      <>
        <Text>{row["foodName"]}</Text>
        <Divider />
      </>
    );
  };
  const viewContent = async () => {
    let sendData = {
      patientDietId: dietSelectId,
      dayNumber: day,
    };

    console.log();
    let data = await fetch(`${host}api/diet/view`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(sendData),
    }).then((response) => {
      return response.json();

      //setDoctors[response.json().value];
    });
    //setFoodList(data);
    //setFoodEaten("");
    //console.log("Doctor and Porcess Id:", data);
    return data;
    //setDiets(data);
  };

  const addContent = async () => {
    let sendData = {
      captureDate: dateToday(),
      patientDietId: dietSelectId,
      dayNumber: day,
      foodName: foodEaten,
    };
    let data = await fetch(`${host}api/diet/capture`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(sendData),
    }).then((response) => {
      return response.json();

      //setDoctors[response.json().value];
    });

    setFoodEaten("");
    setListRefreshed(true);
    console.log("Refresh is ");
    //console.log("Doctor and Porcess Id:", data);
    return data;
    //setDiets(data);
  };

  useEffect(() => {
    //console.log("The Food List is ", foodList.length);
    if (!foodList) {
      console.log("Run the fetch");
      viewContent().then((data) => {
        console.log("Data", data["result"]);
        setFoodList(data["result"]);
      });
      //console.log("The Food List is ", foodList.length);
    }
  }, []);

  useEffect(() => {
    if (listRefreshed) {
      viewContent().then((data) => {
        console.log("Data", data["result"]);
        setFoodList(data["result"]);
        setListRefreshed(false);
      });
    }
  }, [listRefreshed]);

  return (
    <>
      <Container>
        <InputContainer>
          <TextAreaBox>
            <FoodText
              label="What did you eat today ?"
              value={foodEaten}
              onChangeText={(text) => setFoodEaten(text)}
            />
          </TextAreaBox>
          <BtnArea>
            <AddBtn
              mode="contained"
              buttonColor="#3498DB"
              onPress={() => addContent()}
            >
              Add
            </AddBtn>
          </BtnArea>
        </InputContainer>
        <Spacer position="top" size="large" />
        <Spacer position="top" size="large" />
        <ListContainer>
          <FlatList
            data={foodList}
            renderItem={({ item, index }) => {
              return (
                <>
                  <Text>
                    {index + 1}
                    {")"} {item.foodName}
                  </Text>
                  <Divider />
                </>
              );
            }}
            keyExtractor={(item) => item.id.toString()}
          />
        </ListContainer>
      </Container>
    </>
  );
};
