import React, { useState, useEffect } from "react";
import {
  ContainerSaferView,
  Container,
  ContentRule,
  ContentStats,
  Heading,
  Btn,
  HContainer,
  RightContainer,
  LeftContainer,
} from "../components/dietDayRule.Style";
import { SafeAreaView, View, FlatList } from "react-native";
import { Text } from "../../../components/typography/text.component";
import { Spacer } from "../../../components/spacer/spacer.component";
import { Divider } from "react-native-paper";
export const DietDayRuleScreen = ({ navigation, route }) => {
  const [ruleData, setRulesData] = useState([]);
  let dietInfo = {};
  const { data, dietSelectId } = route.params;
  dietInfo = data[1];
  console.log("oBJECT: ", data);
  const {
    day = 0,
    Rules = [],
    yesterdayresult = 0,
    todayresult = 0,
  } = dietInfo;
  console.log("Parmas", dietInfo);
  const textContain = (rules) => {
    console.log(rules);
    return rules.map((row, index) => {
      return (
        <View key={index + 1}>
          <Text>
            {index + 1} {row}
          </Text>
          <Divider />
        </View>
      );
    });
  };

  const captureForm = () => {
    navigation.navigate("captureDiet", {
      day: day,
      dietSelectId: dietSelectId,
    });
  };

  return (
    <Container>
      <Spacer position="top" size="large" />
      <Heading>
        <Text variant="label">Day {day}</Text>
      </Heading>
      <Spacer position="top" size="large" />
      <ContentRule>
        <HContainer>
          {textContain(Rules)}
          {/* <Text>
            1. Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s,
          </Text>
          <Divider />
          <Text>
            2. Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s,
          </Text>
          <Divider />
          <Text>
            3. Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s,
          </Text>
          <Divider />
          <Text>
            4. Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s,
          </Text> */}
        </HContainer>
      </ContentRule>
      <Spacer position="top" size="large" />
      <ContentStats>
        <LeftContainer>
          <Text>Today</Text>
          <Text variant="label">{todayresult}%</Text>
        </LeftContainer>
        <RightContainer>
          <Text>Yesterday</Text>
          <Text variant="label">{yesterdayresult}%</Text>
        </RightContainer>
      </ContentStats>
      <Spacer position="top" size="large" />
      <Btn
        mode="contained"
        onPress={() => {
          captureForm();
        }}
      >
        Capture Meal Details
      </Btn>
    </Container>
  );
};
