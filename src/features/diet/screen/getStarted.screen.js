import React from "react";
import { Container, GetStartedBtn } from "../components/getStarted.style";
import { Text } from "../../../components/typography/text.component";
import { Spacer } from "../../../components/spacer/spacer.component";
export const GetStartedScreen = () => {
  return (
    <>
      <Container>
        <Text variant="heading">
          Your diabetes diet is simply a healthy-eating plan that will help you
          control your blood sugar. Here's help getting started, from meal
          planning to counting carbohydrates.
        </Text>
        <Spacer position="top" size="large" />
        <Text variant="heading">
          A diabetes diet simply means eating the healthiest foods in moderate
          amounts and sticking to regular mealtimes.
        </Text>
        <Spacer position="top" size="large" />
        <Text variant="heading">
          A diabetes diet is a healthy-eating plan that's naturally rich in
          nutrients and low in fat and calories. Key elements are fruits,
          vegetables and whole grains. In fact, a diabetes diet is the best
          eating plan for most everyone.
        </Text>
        <Spacer position="top" size="large" />
        <Spacer position="top" size="large" />
        <GetStartedBtn mode="contained">Get Started</GetStartedBtn>
      </Container>
    </>
  );
};
