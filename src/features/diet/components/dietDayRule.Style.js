import { Button, Card, Title, Paragraph } from "react-native-paper";
import { View } from "react-native";
import styled from "styled-components/native";

export const Container = styled.View`
  background-color: #f0f3f4;
  padding: 15px;
  height: 100%;
  display: flex;
  margin: 2px;
`;
export const Heading = styled.View`
  display: flex;
  background-color: #e5e7e9;
  align-items: center;
  border: 4px solid #e5e7e9;
  border-radius: 4px;
`;
export const ContentRule = styled.View`
  display: flex;
  background-color: #f0f3f4;
  padding: 5px;
  height: 40%;
  border-radius: 5px;
  border: 2px solid #e5e7e9;
`;

export const ContentStats = styled.View`
  display: flex;
  background-color: #85929e;
  flex-direction: row;
  padding: 5px;
  border-radius: 5px;
`;
export const RightContainer = styled.View`
  display: flex;
  width: 40%;
  flex: 1;
  height: 90px;
  background-color: #d0d3d4;
  margin: 5px;
  align-items: center;
  padding-top: 5px;
  border-radius: 5px;
`;

export const LeftContainer = styled.View`
  display: flex;
  flex: 1;
  width: 40%;
  height: 90px;
  background-color: #d0d3d4;
  margin: 5px;
  align-items: center;
  padding-top: 5px;
  border-radius: 5px;
`;
export const Btn = styled(Button)`
  background-color: #145a32;
  height: 45px;
  width: 100%;
`;
export const HContainer = styled.ScrollView``;
