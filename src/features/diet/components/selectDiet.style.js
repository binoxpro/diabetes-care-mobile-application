import {Button} from "react-native-paper";
import { View } from "react-native";
import styled from "styled-components/native";
export const Btn =styled(Button)
`justify-content:center;
background-color:#4CAF50;
border-radius: 35px;
height:70px;
width:19%;
margin:10px;
`
export const BtnArea = styled(View)
`display:flex;
flex-direction:row-reverse;
`