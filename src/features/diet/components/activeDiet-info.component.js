import React from "react";
import { Text } from "../../../components/typography/text.component";
import { FlatList } from "react-native";
import {
  ActiveDiet,
  ActiveDietContent,
  Br,
  Heading,
  SecondHeading,
  ThirdHeading,
} from "../components/activeDiet-info-card.style";

export const ActiveDietInfoCard = ({ activeDietInfo = {} }) => {
  const {
    day = "Today",
    rule = [
      {
        meal: "Breakfast",
        rules: [
          { id: 1, rule: "Rule 1" },
          { id: 2, rule: "Rule 2" },
          { id: 3, rule: "Rule 3" },
        ],
      },
      {
        meal: "Lunch",
        rules: [
          { id: 1, rule: "Rule 1" },
          { id: 2, rule: "Rule 2" },
          { id: 3, rule: "Rule 3" },
        ],
      },
      {
        meal: "Dinner",
        rules: [
          { id: 1, rule: "Rule 1" },
          { id: 2, rule: "Rule 2" },
          { id: 3, rule: "Rule 3" },
        ],
      },
    ],
  } = activeDietInfo;

  const RuleList = ({ rulesl = {} }) => {
    const {
      meal = "Breakfast",
      rules = [
        { id: 1, rule: "Rule 1" },
        { id: 2, rule: "Rule 2" },
        { id: 3, rule: "Rule 3" },
      ],
    } = rulesl;
    return (
      <>
        <SecondHeading>
          <Text variant="heading">{meal}</Text>
        </SecondHeading>

        <FlatList
          data={rules}
          renderItem={({ item }) => {
            return (
              <ThirdHeading>
                <Text>{item.rule}</Text>
              </ThirdHeading>
            );
          }}
          keyExtractor={(item) => item.id.toString()}
        />
      </>
    );
  };

  return (
    <ActiveDiet>
      <ActiveDietContent>
        <Heading>
          <Text variant="heading">Let's follow Our Food Diet </Text>
          <Text variant="heading">{day}</Text>
        </Heading>

        <FlatList
          data={rule}
          renderItem={({ item }) => {
            return <RuleList rulesl={item} />;
          }}
          keyExtractor={(item) => item.meal.toString()}
        />
      </ActiveDietContent>
    </ActiveDiet>
  );
};
