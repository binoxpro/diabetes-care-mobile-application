import { Card, Button } from "react-native-paper";
import { View } from "react-native";
import styled from "styled-components/native";

export const DietCard = styled(Card)`
  margin: ${(props) => props.theme.sizes[0]};
  border-left-width: 4px;
  border-left-color: #4caf50;
  display: flex;
  padding: 0px;
  margin: 10px;
`;
export const Btn = styled(Button)`
  justify-content: center;
  background-color: #4caf50;
  border-radius: 50px;
  height: 60px;
`;

export const BtnCapture = styled(Button)`
  justify-content: center;
  background-color: #e74c3c;
  border-radius: 20px;
  height: 60px;
`;
export const BtnArea = styled(Card.Actions)`
  display: flex;
  flex-direction: row-reverse;
`;
