import { Button, Card, Title, Paragraph } from "react-native-paper";
import { View } from "react-native";
import styled from "styled-components";

export const Container = styled(View)`
  display: flex;
  height: 100%;
  justify-content: flex-start;
  align-items: flex-start;
  padding: ${(props) => props.theme.sizes[3]};
`;
export const GetStartedBtn = styled(Button)`
  width: 100%;
  height: 50px;
  justify-content: center;
  background-color: #58d68d;
`;
