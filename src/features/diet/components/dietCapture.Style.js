import { Button, TextInput } from "react-native-paper";
import { View } from "react-native";
import styled from "styled-components/native";

export const Container = styled.View`
  padding: 15px;
  height: 100%;
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
`;
export const InputContainer = styled.View`
  margin: 5px;
  height: 20%;
  display: flex;
  flex-direction: row;
  margin: 15px;
`;
export const ListContainer = styled.View`
  background-color: #e5e7e9;
  display: flex;
  margin: 5px;
  height: 60%;
  padding: 15px;
  border-radius: 5px;
  border: 1px solid #cacfd2;
`;

export const TextAreaBox = styled.View`
  height: 70px;
  width: 75%;
  border-top: 1px solid #cacfd2;
`;
export const BtnArea = styled.View`
  height: 100%;
  width: 25%;
`;
export const FoodText = styled(TextInput)``;
export const AddBtn = styled(Button)`
  border-right: 1px;
  justify-content: center;
  height: 65px;
  background-color: #2ecc71;
`;
