import {Card,Divider} from "react-native-paper";
import styled from "styled-components/native";
import {View} from "react-native"
 
export const ActiveDiet = styled(Card)
`display:flex;
padding:4px;
margin:8px;
`
export const ActiveDietContent = styled(Card.Content)
`
display:flex;

`
export const Br = styled(Divider)
`
border:2px solid #4CAF50;
`
export const Heading= styled(View)
`
display:flex;
justify-content:center;
align-items:center;
padding-bottom:8px;
`
export const SecondHeading= styled(View)
`
display:flex;
justify-content:center;
align-items:flex-start;
padding:5px;
border-bottom-width:3px;
border-bottom-color:#4CAF50;
width:40%;
`
export const ThirdHeading= styled(View)
`
display:flex;
justify-content:center;
align-items:flex-start;
padding-left:15px;
padding-bottom:3px;
`