import React from "react";
import { Text } from "../../../components/typography/text.component";
import { DietCard, Btn, BtnArea, BtnCapture } from "./dietlist.style";
import { Divider } from "react-native-paper";
import { host } from "../../../utils/env";
export const DietInfoCard = ({ dietInfo = {}, userId = null, clickNow }) => {
  const {
    id = -1,
    dietName = "Low Fats Meal One",
    dietDescription = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    duration = "5",
    btnText = "Start",
    dietSelectId = -1,
  } = dietInfo;

  //btn
  const dateToday = () => {
    let today = new Date();

    let y = today.getFullYear();
    let m = today.getMonth() + 1;
    m = m > 9 ? m : "0" + m;
    let d = today.getDate();
    d = d > 9 ? d : "0" + d;

    return y + "-" + m + "-" + d;
  };
  const doAction = (actionStr) => {
    console.log("You press ", actionStr);
    if (actionStr == "Start") {
      //track the start
      selectDietPlan();
    } else {
      //move the
      console.log("Click Now Running ", actionStr);
      clickNow(dietInfo);
    }
  };

  const selectDietPlan = async () => {
    let sendData = {
      patientId: userId,
      dietId: dietInfo.id,
      startDate: dateToday(),
    };
    let data = await fetch(`${host}api/diet/select`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(sendData),
    }).then((response) => {
      return response.json();

      //setDoctors[response.json().value];
    });
  };

  return (
    <DietCard>
      <BtnArea>
        {btnText === "Start" ? (
          <Btn
            mode="contained"
            onPress={() => {
              doAction(btnText);
            }}
          >
            {btnText}
          </Btn>
        ) : btnText === "Capture" ? (
          <BtnCapture
            mode="contained"
            onPress={() => {
              doAction(btnText);
            }}
          >
            {btnText}
          </BtnCapture>
        ) : (
          <></>
        )}
      </BtnArea>
      <DietCard.Content>
        <Text variant="heading">
          <Text variant="titleDiet">Diet : </Text>
          {dietName}
        </Text>
        <Divider />
        <Text variant="heading">Description:{dietDescription}</Text>
        <Divider />
        <Text variant="heading">Duration:{duration} Days</Text>
      </DietCard.Content>
    </DietCard>
  );
};
