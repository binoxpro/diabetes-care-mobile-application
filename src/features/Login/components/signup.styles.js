import {
  TextInput,
  Title,
  Button,
  List,
  ActivityIndicator,
} from "react-native-paper";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import {
  View,
  Image,
  SafeAreaView,
  StatusBar,
  ImageBackground,
} from "react-native";
import DatePicker from "react-native-datepicker";
import styled from "styled-components/native";
export const TopContainer = styled(KeyboardAwareScrollView)`
  width: 100%;
  margin-top: 2px;
`;

export const SafeArea = styled(SafeAreaView)`
  ${StatusBar.currentHeight && `margin-top:${StatusBar.currentHeight}px`}px;
  margin-top: 15px;
  display: flex;
`;
export const ProgressN = styled(ActivityIndicator)``;
export const Container = styled.View`
  display: flex;
  padding-left: 5px;
  padding-right: 5px;
  height: 100%;
`;
export const HeaderContainer = styled.View`
  display: flex;
  flex-direction: row;
  height: 20%;
  justify-content: center;
  align-items: center;
  margin-left: ${(props) => props.theme.space[3]};
  margin-right: ${(props) => props.theme.space[3]};
`;
export const LogoText = styled.View`
  height: 100%;
  width: 70%;
  justify-content: center;
`;
export const LogoLine = styled.View`
  height: 5%;
  width: 30%;
  background-color: ${(props) => props.theme.colors.bg.black};
`;

export const DatePickerN = styled(DatePicker)`
  width: 80%;
`;
export const Frm = styled.View`
  display: flex;
  flex-direction: column;
  margin-left: ${(props) => props.theme.space[1]};
  margin-right: ${(props) => props.theme.space[1]};
  padding: ${(props) => props.theme.space[3]};
  border-radius: ${(props) => props.theme.sizes[0]};
`;

export const FrmRow = styled.View`
  display: flex;
  flex-direction: row;
  margin-left: ${(props) => props.theme.space[3]};
  padding: ${(props) => props.theme.space[4]};
  border-radius: ${(props) => props.theme.sizes[0]};
`;
export const FrmAction = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;

  padding: ${(props) => props.theme.space[2]};
`;
export const FrmActionColumn = styled.View`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin-left: ${(props) => props.theme.space[3]};
`;

export const TxtInput = styled(TextInput)`
  margin-bottom: 10px;
  height: 50px;
  font-family: ${(props) => props.theme.fonts.body};
`;
export const FooterContainer = styled.View`
  margin-top: 15px;
  align-items: center;
  justify-content: center;
`;

export const Btn = styled(Button)`
  flex: 1;
  height: 50px;
  background-color: #145a32;
  padding-top: 5px;
`;

export const BtnSm = styled(Button)`
  height: 50px;
  width: 100%;
  background-color: #2b60de;
  margin-top: ${(props) => props.theme.sizes[2]};
`;

export const Btnrtl = styled(Button)`
  flex: 1;
  height: 50px;
  background-color: ${(props) => props.theme.colors.ui.error};
  border-radius: 5px;
  font-family: ${(props) => props.theme.fonts.heading};
  box-shadow: 5px 5px 10px grey;
  justify-content: center;
`;
