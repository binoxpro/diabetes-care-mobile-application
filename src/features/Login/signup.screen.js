import React, { useState, useContext } from "react";
import { ScrollView, ActivityIndicator, TouchableOpacity } from "react-native";
import { Colors } from "react-native-paper";
import DatePicker from "react-native-datepicker";
import { mconfig } from "../../utils/utils";
import { Spacer } from "../../components/spacer/spacer.component";
import { Text } from "../../components/typography/text.component";
import {
  TopContainer,
  DatePickerN,
  Container,
  HeaderContainer,
  Frm,
  FrmAction,
  FooterContainer,
  TxtInput,
  SafeArea,
  Btn,
  ProgressN,
} from "./components/signup.styles";
import { host } from "../../utils/env";

import { AuthenticationContext } from "../../services/authentication/authentication.context";

// onPress={handleSelection("Others")}
export const SignUpScreen = ({ navigation }) => {
  const [user, setUser] = useState([]);
  const [professionStr, setProfessionStr] = useState(
    "Please Select Profession"
  );
  const [loading, setLoading] = useState(false);
  const [nameStr, setNameStr] = useState("");
  const [mobileNumberStr, setMobileNumberStr] = useState("");
  const [emailStr, setEmailStr] = useState("");
  const [passwordStr, setPasswordStr] = useState("");
  const [cpasswordStr, setCPasswordStr] = useState("");
  const [expanded, setExpanded] = useState(false);
  const [date, setDate] = useState("1991-01-01");
  const [responseStr, setResponseStr] = useState([]);

  const { onSignUp, isLoading, error, isVerified } = useContext(
    AuthenticationContext
  );

  const signUp = async () => {
    setLoading(true);
    let fullName = nameStr.split(" ");
    let result = await fetch(`${host}users/register/`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        firstName: fullName[0],
        lastName: fullName[1],
        dob: date,
        mobileNumber: mobileNumberStr,
        pword: passwordStr,
      }),
    }).then((res) => {
      setLoading(false);
      return res.json();
    });
    navigation.navigate("login");
  };

  return (
    <SafeArea>
      <Spacer position="top" size="large" />
      <TopContainer>
        <ScrollView>
          <Container>
            <Frm>
              <Spacer position="top" size="large" />
              {error && <Text label="error">{error}</Text>}
              <TxtInput
                label="Name"
                value={nameStr}
                onChangeText={(text) => setNameStr(text)}
              />
              <TxtInput
                label="Mobile Number"
                value={mobileNumberStr}
                onChangeText={(text) => setMobileNumberStr(text)}
              />
              <TxtInput
                label="Date of Birth"
                value={date}
                onChangeText={(text) => setDate(text)}
              />

              <TxtInput
                label="Password"
                secureTextEntry={true}
                value={passwordStr}
                onChangeText={(text) => setPasswordStr(text)}
              />
              <TxtInput
                label="Confirm Password"
                secureTextEntry={true}
                value={cpasswordStr}
                onChangeText={(text) => setCPasswordStr(text)}
              />
              <FrmAction>
                {!loading ? (
                  <Btn
                    mode="contained"
                    onPress={() => {
                      signUp();
                    }}
                  >
                    Sign Up
                  </Btn>
                ) : (
                  <ProgressN animating={true} color={Colors.lightGreen50} />
                )}
              </FrmAction>
              <FooterContainer>
                <TouchableOpacity>
                  <Text>I have an account? Login</Text>
                </TouchableOpacity>
              </FooterContainer>
            </Frm>
          </Container>
        </ScrollView>
      </TopContainer>
    </SafeArea>
  );
};
