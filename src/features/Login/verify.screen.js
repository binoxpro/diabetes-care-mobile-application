import React, { useState, useContext } from "react";

import { Image } from "react-native";
import { Colors } from "react-native-paper";
import { mconfig } from "../../utils/utils";
import {
  Container,
  HeaderContainer,
  LogoImage,
  Frm,
  FrmVerify,
  TxtInput,
  FrmRow,
  FrmAction,
  FrmActionColumn,
  Btn,
  FooterContainer,
  BtnSm,
  ProgressN,
} from "./components/login.styles";

import { Text } from "../../components/typography/text.component";
import { Spacer } from "../../components/spacer/spacer.component";
import { AuthenticationContext } from "../../services/authentication/authentication.context";

export const VerifyScreen = ({ navigation }) => {
  const [verificationCode, setVerificationCode] = useState();
  const [passwordx, setPasswordx] = useState();
  const [confirmPassword, setConfirmPassword] = useState();
  const { onVerification, isLoading, error } = useContext(
    AuthenticationContext
  );
  console.log("The User ", isLoading, error);
  return (
    <Container>
      <HeaderContainer>
        <Text variant="body">
          Please type the verifcation code sent to your mobile number and Create
          your access password
        </Text>
      </HeaderContainer>
      <FrmVerify>
        <TxtInput
          label="Verification Code"
          value={verificationCode}
          maxLength={5}
          onChangeText={(text) => setVerificationCode(text)}
        />
        <TxtInput
          label="Password"
          secureTextEntry={true}
          value={passwordx}
          onChangeText={(text) => setPasswordx(text)}
        />
        <TxtInput
          label="Confirm Password"
          secureTextEntry={true}
          value={confirmPassword}
          onChangeText={(text) => setConfirmPassword(text)}
        />
        {isLoading && (
          <ProgressN animating={true} color={Colors.lightGreen50} />
        )}

        <BtnSm
          mode="contained"
          onPress={() => onVerification(verificationCode, passwordx)}
        >
          Verify
        </BtnSm>
        <BtnSm mode="contained">Resend Code</BtnSm>
      </FrmVerify>
    </Container>
  );
};
