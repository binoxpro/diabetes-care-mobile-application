import React, { useState, useContext } from "react";
import { Colors } from "react-native-paper";
import { AuthenticationContext } from "../../services/authentication/authentication.context";
import {
  Container,
  HeaderContainer,
  LogoImage,
  FrmAction,
  Frm,
  FooterContainer,
  SafeArea,
  TxtInput,
  Btn,
  BtnSignUp,
  Btnrtl,
  LoginBackground,
  LogoText,
  LogoLine,
  ProgressN,
} from "./components/login.styles";
import { Spacer } from "../../components/spacer/spacer.component";
import { Text } from "../../components/typography/text.component";

export const LoginScreen = ({ navigation }) => {
  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const { onLogin, error, isLoading } = useContext(AuthenticationContext);
  const clearFields = () => {};
  return (
    <SafeArea>
      <LoginBackground>
        <Container>
          <Spacer position="top" size="large" />
          <Spacer position="top" size="large" />
          <HeaderContainer>
            <LogoText>
              <Text variant="title">DCARE</Text>
            </LogoText>
          </HeaderContainer>
          <Frm>
            <TxtInput
              label="Username"
              value={username}
              onChangeText={(text) => setUserName(text)}
            />
            <TxtInput
              label="Password"
              secureTextEntry={true}
              value={password}
              onChangeText={(text) => setPassword(text)}
            />
            {error && <Text variant="error">{error}</Text>}
            {isLoading && (
              <ProgressN animating={true} color={Colors.lightGreen50} />
            )}
            <FrmAction>
              <Btn
                mode="contained"
                onPress={() => {
                  //console.log("Pressed: ", email, password);
                  onLogin(username, password);
                }}
              >
                Login
              </Btn>
              <Spacer position="right" size="large" />
              <BtnSignUp
                mode="contained"
                onPress={() => {
                  //console.log("Pressed: ", email, password);
                  navigation.navigate("SignUp");
                }}
              >
                Sign Up
              </BtnSignUp>
            </FrmAction>
            <FooterContainer>
              <Text>All Rights reserverd</Text>
            </FooterContainer>
          </Frm>
        </Container>
      </LoginBackground>
    </SafeArea>
  );
};
