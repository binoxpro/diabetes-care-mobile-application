import React from "react";
import {Text} from "../../../components/typography/text.component"
import {Form,FormAction,TxtInput} from "./personinfo.style"

export const PersonInfoCard=({info})=>{
    const{
        status="true",
        profile={
            firstName= "James",
            lastName= "Yiga",
            dob= "1990-12-31T21:00:00.000Z",
            mobileNumber= "0781587089"
        },
        bodyMassIndex={
            height= 0,
            weight= 0
        },
        medical= {
            ancestor= -1,
            type1Diabetes= -1,
            type2Diabetes= -1
        }
    }=info;
    return (
        <Form>
            <TxtInput 
                type='outlined'
                label="firstName"
                value={info.profile.firstName}
                onchangeText={text=>(info.profile.firstName=text)}
            />
        </Form>
    )
}