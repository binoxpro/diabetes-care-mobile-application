import {
TextInput,
  Button,
  ActivityIndicator,
  } from "react-native-paper";
  import {
    View,
  } from "react-native";
  import styled from "styled-components/native";

  export const Form =styled.View`
  display:flex;
  `
  export const FormAction = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  padding: ${(props) => props.theme.space[2]};
`;

export const TxtInput = styled(TextInput)`
  margin-bottom: 10px;
  height:50px;
  font-family: ${(props) => props.theme.fonts.body};
`;




  
