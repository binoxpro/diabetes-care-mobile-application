import { TextInput } from "react-native-paper";
import {
  View,
  Image,
  SafeAreaView,
  StatusBar,
  ImageBackground,
} from "react-native";

import { ScrollView } from "react-native-virtualized-view";

import styled from "styled-components/native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

export const TopContainer = styled(ScrollView)`
  width: 100%;
  margin-top: 2px;
`;
export const SafeArea = styled(SafeAreaView)`
  ${StatusBar.currentHeight && `margin-top:${StatusBar.currentHeight}px`}px;
  margin-top: 15px;
  display: flex;
`;
export const Container = styled.View`
  display: flex;
  padding-left: 5px;
  padding-right: 5px;
`;

export const ProfileImage = styled.View`
display: flex;
height:200px;
align-items: center;
`;

export const TabContainer = styled.View`
  display: flex;
  height: 450px;
  padding: 2px;
  margin-top: 5px;
`;
export const TabViewContainer = styled.View`
  display: flex;
  padding: 20px;
`;
export const TxtInput = styled(TextInput)`
  margin-bottom: 10px;
  height: 40px;
  font-family: ${(props) => props.theme.fonts.body};
`;
