import React, { useState, useContext, useEffect } from "react";
import { Picker } from "@react-native-picker/picker";
import DropDownPicker from "react-native-dropdown-picker";
import {
  TopContainer,
  Container,
  ProfileImage,
  TabContainer,
  TabViewContainer,
} from "../components/profile.style";
import { View, useWindowDimensions } from "react-native";
import { Switch, Button } from "react-native-paper";
import { TabView, SceneMap } from "react-native-tab-view";
import { Avatar } from "react-native-paper";
import { Btn, SafeArea, TxtInput } from "../../Login/components/login.styles";
import { Spacer } from "../../../components/spacer/spacer.component";
import { Text } from "../../../components/typography/text.component";
import { ProfileContext } from "../../../services/profile/profile.context";

export const UserProfile = () => {
  const layout = useWindowDimensions();
  const [index, setIndex] = useState(0);
  const [info, setInfo] = useState(null);
  const [firstName, setFirstName] = useState(null);
  const [lastName, setLastName] = useState(null);
  const [dob, setDob] = useState(null);
  const [mobileNumber, setMobileNumber] = useState(null);
  const [height, setHeight] = useState(null);
  const [weight, setWeight] = useState(null);
  const [profileArr, setProfileArr] = useState(null);
  const [selectFreshVIL, setSelectFreshVIL] = useState();

  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);
  const [items, setItems] = useState([
    { label: "25%", value: "25" },
    { label: "50%", value: "50" },
    { label: "75%", value: "75" },
  ]);

  const [isAncestorSwitch, setIsAncestorSwitch] = useState(false);
  const [isType1DiabetesSwitch, setIsType1DiabetesSwitch] = useState(false);
  const [isType2DiabetesSwitch, setIsType2DiabetesSwitch] = useState(false);

  const onToggleSwitch = () => setIsAncestorSwitch(!isAncestorSwitch);
  const onToggleSwitch1 = () =>
    setIsType1DiabetesSwitch(!isType1DiabetesSwitch);
  const onToggleSwitch2 = () =>
    setIsType2DiabetesSwitch(!isType2DiabetesSwitch);

  const { profile, saveProfile, isLoading, error, riskFactor } =
    useContext(ProfileContext);

  const prof = {
    profile: "",
    userId: "",
    weight: "",
    height: "",
    ancestor: -1,
    fvil: 0,
    type1Diabetes: -1,
    type2Diabetes: -1,
  };

  const save = () => {
    prof.profile = "medical";
    prof.weight = weight;
    prof.height = height;
    prof.ancestor = 1;
    prof.fvil = value;
    prof.type1Diabetes = isType1DiabetesSwitch ? 1 : 0;
    prof.type2Diabetes = isType2DiabetesSwitch ? 1 : 0;
    saveProfile(prof);
  };

  const LoadForm = () => {
    if (profile) {
      console.log(profile["profile"]["personal"]["firstName"]);
      if (profile["profile"]["personal"]["firstName"]) {
        setFirstName(profile["profile"]["personal"]["firstName"]);
      }
      if (profile["profile"]["personal"]["lastName"]) {
        setLastName(profile["profile"]["personal"]["lastName"]);
      }
      if (profile["profile"]["personal"]["dob"]) {
        setDob(profile["profile"]["personal"]["dob"]);
      }
      if (profile["profile"]["personal"]["mobileNumber"]) {
        setMobileNumber(profile["profile"]["personal"]["mobileNumber"]);
      }

      if (profile["profile"]["bodyMassIndex"]["height"]) {
        setHeight(profile["profile"]["bodyMassIndex"]["height"]);
      }

      if (profile["profile"]["bodyMassIndex"]["weight"]) {
        setWeight(profile["profile"]["bodyMassIndex"]["weight"]);
      }
      //console.log("Your ancestors",profile['profile']['medical']["ancestor"]);
      if (profile["profile"]["medical"]["ancestor"]) {
        console.log(
          "Your ancestors",
          profile["profile"]["medical"]["ancestor"]
        );
        if (profile["profile"]["medical"]["ancestor"] >= 0) {
          setIsAncestorSwitch(
            getToggleVal(profile["profile"]["medical"]["ancestor"])
          );
          onToggleSwitch();
        }

        //setWeight(profile['profile']['bodyMassIndex']["weight"])
      }

      if (profile["profile"]["medical"]["type1Diabetes"]) {
        //console.log("Your ancestors",profile['profile']['medical']["type1Diabetes"]);
        if (profile["profile"]["medical"]["type1Diabetes"] >= 0) {
          setIsAncestorSwitch(
            getToggleVal(profile["profile"]["medical"]["type1Diabetes"])
          );
          onToggleSwitch1();
        }

        //setWeight(profile['profile']['bodyMassIndex']["weight"])
      }

      if (profile["profile"]["medical"]["type2Diabetes"]) {
        console.log(
          "Your ancestors",
          profile["profile"]["medical"]["type2Diabetes"]
        );
        if (profile["profile"]["medical"]["type2Diabetes"] >= 0) {
          setIsAncestorSwitch(
            getToggleVal(profile["profile"]["medical"]["type2Diabetes"])
          );
          onToggleSwitch2();
        }

        //setWeight(profile['profile']['bodyMassIndex']["weight"])
      }
    } else {
      console.log("Not loaded");
    }
  };

  const getToggleVal = (i) => {
    console.log("The Toggle is ", i);
    return i === 1 ? true : false;
  };

  const [routes] = useState([
    { key: "first", title: "Personal" },
    { key: "second", title: "Medical" },
  ]);

  //setInfo(PersonInfo);

  const FirstRoute = () => (
    <TabViewContainer>
      <Container>
        <TxtInput
          mode="outlined"
          label="First Name"
          value={firstName}
          onChangeText={(text) => setFirstName(text)}
        />
        <TxtInput
          mode="outlined"
          label="Last Name"
          value={lastName}
          onChangeText={(text) => setLastName(text)}
        />
        <TxtInput
          mode="outlined"
          label="Date of Birth"
          value={dob}
          onChangeText={(text) => setDob(text)}
        />
        <TxtInput
          mode="outlined"
          label="Mobile Number"
          value={mobileNumber}
          onChangeText={(text) => setMobileNumber(text)}
        />
      </Container>
    </TabViewContainer>
  );

  const SecondRoute = () => (
    <TabViewContainer>
      <Container>
        <TxtInput
          mode="outlined"
          label="Height"
          value={height}
          onChangeText={(text) => setHeight(text)}
        />
        <TxtInput
          mode="outlined"
          label="Weight"
          value={weight}
          onChangeText={(text) => setWeight(text)}
        />
        <Text>What is your fresh vegetable intake Level? </Text>
        <DropDownPicker
          open={open}
          value={value}
          items={items}
          setOpen={setOpen}
          setValue={setValue}
          setItems={setItems}
        />
        <Text>Do you have Type1Diabetes ? </Text>
        <Switch value={isType1DiabetesSwitch} onValueChange={onToggleSwitch1} />
        <Text>Do you have Type2Diabetes ? </Text>
        <Switch value={isType2DiabetesSwitch} onValueChange={onToggleSwitch2} />

        {/* <Picker
          selectedValue={selectFreshVIL}
          onValueChange={(itemValue, itemIndex) => {
            setSelectFreshVIL(itemValue);
          }}
        >
          <Picker.Item label="25%" value="25" />
          <Picker.Item label="50%" value="50" />
          <Picker.Item label="75%" value="75" />
        </Picker> */}
      </Container>
    </TabViewContainer>
  );

  const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
  });

  //LoadForm();

  useEffect(() => {
    if (!info) {
      LoadForm();
      setInfo(true);
    }
  }, [setInfo]);

  return (
    <SafeArea>
      <TopContainer>
        <Container>
          <ProfileImage>
            <Avatar.Image
              size={150}
              source={require("../../../../assets/avatar_new.png")}
            />
            <Text>
              {firstName} {lastName}
            </Text>
            <Text variant="heading">Risk Factor : {riskFactor}</Text>
          </ProfileImage>
          <TabContainer>
            <TabView
              navigationState={{ index, routes }}
              renderScene={renderScene}
              onIndexChange={setIndex}
              initialLayout={{ width: layout.width }}
            />
          </TabContainer>
        </Container>
        <Container>
          <Button
            mode="text"
            onPress={() => {
              save();
            }}
          >
            Edit
          </Button>
        </Container>
      </TopContainer>
    </SafeArea>
  );
};
