import React, { useState, useContext } from "react";
import { TouchableOpacity, Alert, RefreshControl } from "react-native";

import { DtestInfoCard } from "../components/dtest-info-card.component";
import {
  DtestList,
  DtestSearchBar,
  LoadContainer,
  Loader,
} from "../components/dtest.style";
import { Text } from "../../../components/typography/text.component";
import { DiabeticTestContext } from "../../../services/diabetictest/diabetictest.context";

export const DtestScreen = ({ navigation }) => {
  const [searchQuery, setSearchQuery] = useState();
  //const [isLoading, setIsLoadding] = useState(false);

  const { diabeticTests, isLoading, error } = useContext(DiabeticTestContext);

  const getNextView = ({ item }) => {
    navigation.navigate("DiabeteTestCaptureData", {
      testId: item.id,
      testName: item.testName,
    });
  };

  return (
    <>
      <DtestSearchBar
        placeholder="Search Test Type"
        value={searchQuery}
        onChangeText={() => {}}
      />
      {isLoading && (
        <LoadContainer>
          <Loader animating={true} />
        </LoadContainer>
      )}
      {diabeticTests && (
        <DtestList
          data={diabeticTests}
          renderItem={({ item }) => {
            return (
              <TouchableOpacity
                onPress={() => {
                  getNextView({ item });
                }}
              >
                <DtestInfoCard testInfo={item} />
              </TouchableOpacity>
            );
          }}
          keyExtractor={(item) => item.id.toString()}
        />
      )}
    </>
  );
};
