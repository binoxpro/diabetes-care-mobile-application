import React, { useState, useContext } from "react";
import { ScrollView } from "react-native";
import { Divider } from "react-native-paper";
import DropDownPicker from "react-native-dropdown-picker";

import { Text } from "../../../components/typography/text.component";
import { Spacer } from "../../../components/spacer/spacer.component";

import {
  Container,
  Frm,
  TxtInput,
  BtnSm,
  TopContainer,
} from "../components/dtest-capturedata.style";
import { DiabeticTestContext } from "../../../services/diabetictest/diabetictest.context";

export const DtestCaptureDataScreen = ({ navigate, route }) => {
  const [sugarValue, setSugarValue] = useState();
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);
  const [items, setItems] = useState([
    { label: "mg/dl", value: "mg/dl" },
    { label: "mmol/l", value: "mmol/l" },
    { label: "AIC(%)", value: "AIC(%)" },
  ]);

  const { isLoading, error, saveResult } = useContext(DiabeticTestContext);

  const { testId, testName } = route.params;
  console.log("The Values", testId, " ", testName);
  return (
    <Container>
      <TopContainer>
        <Frm>
          <Text variant="lists">
            Please record your test Results in abelow.
          </Text>
          <Text variant="lists">{testName}</Text>
          <Spacer position="top" size="large" />
          <Divider />
          <Text>Measurement Unit:</Text>
          <DropDownPicker
            open={open}
            value={value}
            items={items}
            setOpen={setOpen}
            setValue={setValue}
            setItems={setItems}
          />
          <Spacer position="top" size="large" />
          <TxtInput
            label="Glucose Test Reult"
            value={sugarValue}
            onChangeText={(text) => setSugarValue(text)}
          />
          <BtnSm onPress={saveResult(testId, value, sugarValue)}>
            Submit Result
          </BtnSm>
        </Frm>
      </TopContainer>
    </Container>
  );
};
