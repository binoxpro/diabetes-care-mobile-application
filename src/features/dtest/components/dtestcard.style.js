import { Button, Card, Title, Paragraph } from "react-native-paper";
import { View } from "react-native";
import styled from "styled-components/native";

export const TestInfoCard = styled(Card)`
  margin: ${(props) => props.theme.sizes[0]};
  border-left-width: 5px;
  border-left-color: red;
`;
export const BtnCapture = styled(Button)`
  height: 50px;
  width: 100%;
  background-color: #2b60de;
  margin-top: ${(props) => props.theme.sizes[2]};
`;
export const InfoContainer = styled(View)`
  flex-direction: row;
  /*border: 1px solid black;*/
`;
export const DetailContainer = styled(View)`
  display: flex;
  width:100%;
  height:150px;
  background-color: #f5f5f5;
  margin-top: ${(props) => props.theme.sizes[0]};
  padding: ${(props) => props.theme.sizes[0]};
`;
export const InstructionContainer = styled(View)`
  display: flex;
  background-color: #f5f5f5;
  margin-top: ${(props) => props.theme.sizes[0]};
  padding: ${(props) => props.theme.sizes[0]};
`;
export const HeaderContainer = styled(View)`
  display: flex;
  flex-direction: row;
  border-left-width: 5px;
  border-left-color: #795548;
  margin-bottom: ${(props) => props.theme.sizes[0]};
  background-color: #f5f5f5;
  padding: 4px;
`;
export const HeaderTextArea = styled(View)`
  width: 70%;
`;
export const HeaderBtnArea = styled(View)`
  width: 30%;
  /*background-color: ${(props) => props.theme.colors.bg.pink};*/
`;
export const HoriztionalLine = styled(View)`
  height: 2px;
  border-bottom: 1px solid black;
`;
