import styled from "styled-components/native";
import { View, TouchableOpacity, Image } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { TextInput, Title, Button } from "react-native-paper";

export const Container = styled(View)`
  display: flex;
  padding-left: 5px;
  padding-right: 5px;
  height: 100%;
  margin-top:10%;
`;
export const TopContainer = styled(KeyboardAwareScrollView)`
  width: 100%;
  margin-top: 2px;
`;
export const Frm = styled.View`
  display: flex;
  flex-direction: column;
  margin-left: ${(props) => props.theme.space[3]};
  margin-right: ${(props) => props.theme.space[3]};
  padding: ${(props) => props.theme.space[4]};
  border-radius: ${(props) => props.theme.sizes[0]};
  background-color: ${(props) => props.theme.colors.bg.secondary};
`;
export const TxtInput = styled(TextInput)`
  margin-bottom: 10px;
  height: 50px;
  font-family: ${(props) => props.theme.fonts.body};
`;
export const BtnSm = styled(Button)`

    flex: 1;
  height: 40px;
  width: 100%;
  background-color:  #145a32;
  margin-top: ${(props) => props.theme.sizes[2]};
  justify-content: center;
  color:#fff;
`;
export const FrmAction = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;

  padding: ${(props) => props.theme.space[2]};
`;
export const Btn = styled(TouchableOpacity)`


  height: 50px;
  width: 100%;
  background-color: #2b60de;
  margin-top: ${(props) => props.theme.sizes[2]};
  justify-content: center;
  align-items: center;
`;
export const ImageContainer = styled(View)`
  display: flex;
  width: 100%;
  margin-top: ${(props) => props.theme.sizes[2]};
`;
export const SignImage = styled(Image)`
  display: flex;
`;
