import styled from "styled-components/native";
import { FlatList, View } from "react-native";
import { Searchbar, ActivityIndicator } from "react-native-paper";
export const DtestList = styled(FlatList).attrs({
  contentContainerStyle: {
    padding: 16,
  },
})``;
export const DtestSearchBar = styled(Searchbar)`
  margin: ${(props) => props.theme.sizes[2]};
`;
export const LoadContainer = styled(View)``;
export const Loader = styled(ActivityIndicator)``;