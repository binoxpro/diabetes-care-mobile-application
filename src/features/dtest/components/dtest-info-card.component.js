import React from "react";
import { List } from 'react-native-paper';
import { Text } from "../../../components/typography/text.component";
import { TestCard } from "../../../components/card.component";
import {
    TestInfoCard,
    BtnCapture,
    InfoContainer,
    DetailContainer,
    InstructionContainer,
    HeaderContainer,
    HeaderTextArea,
    HeaderBtnArea,
} from"./dtestcard.style"



export const DtestInfoCard =({testInfo = {}})=>{
    const {
        id=-1,
        testName="Test Information",
        testDescription="Test Information"
    }=testInfo;

    

    return (
        <TestInfoCard>
            <TestInfoCard.Content>
                <HeaderContainer>
                    <HeaderTextArea>
                        <Text variant="caption">{testName}</Text>
                    </HeaderTextArea>
                </HeaderContainer>
                <DetailContainer>
                <Text variant="lists">{testDescription}</Text>
                </DetailContainer>
            </TestInfoCard.Content>
        </TestInfoCard>
    )

}