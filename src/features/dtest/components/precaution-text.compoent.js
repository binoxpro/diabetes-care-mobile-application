import React from "react";
import { FlatList } from "react-native";
import { List,Divider } from 'react-native-paper';
import {Text} from '../../../components/typography/text.component'

export const PreCautionText=({dtests={}})=>{
   
    const {
        text="text",
        id="0"
    }=dtests

    
    return(
        <FlatList
        data={dtests}
        renderItem=
        {
            ({item})=>
            {
                return(
                    <Text variant="lists">{item.text}</Text>
                        )
            }
        }
        keyExtractor={(item)=>item.id}
    />
    )
}
