import React, { useContext, useState } from "react";
import styled from "styled-components/native";
import { View } from "react-native";
import { Divider, ActivityIndicator, Colors } from "react-native-paper";
import {
  SafeArea,
  Container,
  HeaderContainer,
  Frm,
  FrmAction,
  AnswerContainer,
} from "./component/question.styles";
import { Text } from "../../components/typography/text.component";

import { Answer } from "../answer/Answer";
import { Btn } from "../Login/components/login.styles";
import { Spacer } from "../../components/spacer/spacer.component";
import { QuestionContext } from "../../services/question/question.context";
import { questionRequest } from "../../services/question/question.service";

export const Question = ({ route }) => {
  //console.log("Item " + questionItem["qnsAnswer"].length);

  const { questionId, status } = route.params;
  const { isLoading, error, questions } = useContext(QuestionContext);
  //console.log(questionContext);

  return (
    <>
      <SafeArea>
        {isLoading && (
          <View style={{ position: "absolute", top: "50%", left: "50%" }}>
            <ActivityIndicator
              size={50}
              style={{ marginLeft: -25 }}
              animating={true}
              color={Colors.blue300}
            />
          </View>
        )}
        <Container>
          <FrmAction>
            <Btn mode="contained">Back</Btn>
            <Spacer position="right" size="large" />
            <Text>
              Question {questions["qnsNo"]} of {questions["qnsNoTotal"]}
            </Text>
            <Spacer position="left" size="large" />
            <Btn mode="contained">Next</Btn>
          </FrmAction>
          <HeaderContainer>
            {/* <Text style={{ fontSize: 25, color: "black", fontWeight: "bold" }}> */}

            {/* </Text> */}
            <Divider />
          </HeaderContainer>
          <Frm>
            <Text variant="title">{questions["qnsContent"]}</Text>
            {/* </Text> */}
            <AnswerContainer>{Answer(questions["qnsAnswer"])}</AnswerContainer>
          </Frm>
        </Container>
      </SafeArea>
    </>
  );
};
