//import "react-native-gesture-handler";
import React, { useEffect, useState } from "react";
import { ThemeProvider } from "styled-components";
import { theme } from "./src/infrastructure/theme";
import {
  useFonts as useOswald,
  Oswald_400Regular,
} from "@expo-google-fonts/oswald";
import { useFonts as useLato, Lato_400Regular } from "@expo-google-fonts/lato";
import { Navigation, Navigator } from "./src/infrastructure/navigation";

import ExpoStatusBar from "expo-status-bar/build/ExpoStatusBar";

import { AuthienticationContextProvider } from "./src/services/authentication/authentication.context";

// const firebaseConfig = {
//   apiKey: "AIzaSyCWatejTr8qex8bvEfXbzwQtIGNAjINaH0",
//   authDomain: "justicehive.firebaseapp.com",
//   projectId: "justicehive",
//   storageBucket: "justicehive.appspot.com",
//   messagingSenderId: "1022224418335",
//   appId: "1:1022224418335:web:d88084a550a4fa8bc0772a",
// };

// Initialize Firebase

//firebase.initializeApp(firebaseConfig);

export default function App() {
  global.apiUrl = "https://dcaredemo.herokuapp.com/";

  const [oswaldLoaded] = useOswald({
    Oswald_400Regular,
  });
  const [latoLoaded] = useLato({
    Lato_400Regular,
  });

  if (!oswaldLoaded || !latoLoaded) {
    return null;
  }

  return (
    <>
      <ThemeProvider theme={theme}>
        <AuthienticationContextProvider>
          <Navigation />
        </AuthienticationContextProvider>
        <ExpoStatusBar style="auto" />
      </ThemeProvider>
    </>
  );
}
//"react-native-signature-canvas": "^1.7.1",
// "react-native-web": "^0.11.7",
